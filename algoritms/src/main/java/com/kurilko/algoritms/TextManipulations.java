package com.kurilko.algoritms;

public class TextManipulations {
    public static void swap(int[] ints, int a, int b) {
        int temp;
        temp = ints[a];
        ints[a] = ints[b];
        ints[b] = temp;
    }
}

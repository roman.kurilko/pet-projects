package com.kurilko.algoritms;

public class QuickSort {

    public static void sort(int[] ints, boolean direction) {
        quickSort(ints, 0, ints.length, direction);
    }

    private static void quickSort(int[] ints, int start, int end, boolean direction) {
        if (start >= end || ints.length == 0)
            return;
        int left = start;
        int right = end - 1;
        int middle = left + (right - left) / 2;
        while (left < right) {
            if (direction) {
                while (left < middle && ints[left] <= ints[middle]) {
                    left++;
                }
                while (right > middle && ints[right] >= ints[middle]) {
                    right--;
                }
            } else {
                while (left < middle && ints[left] >= ints[middle]) {
                    left++;
                }
                while (right > middle && ints[right] <= ints[middle]) {
                    right--;
                }
            }
            if (left <= right) {
                if (direction) {
                    TextManipulations.swap(ints, right, left);
                } else {
                    TextManipulations.swap(ints, left, right);
                }
                if (left == middle) {
                    middle = right;
                } else if (right == middle) {
                    middle = left;
                }
            }
        }
        quickSort(ints, start, middle, direction);
        quickSort(ints, middle + 1, end, direction);
    }
}

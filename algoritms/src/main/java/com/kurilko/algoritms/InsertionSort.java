package com.kurilko.algoritms;


public class InsertionSort {
    private static void sort(int[] ints) {
        for (int i = 0; i < ints.length; i++) {
            for (int j = 0; j < i; j++) {
                if (ints[i] < ints[j]) {
                    TextManipulations.swap(ints, i, j);
                }
            }
        }
    }
}

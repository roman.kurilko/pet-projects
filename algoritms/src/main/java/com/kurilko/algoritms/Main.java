package com.kurilko.algoritms;

public class Main {
    public static void main(String[] args) {
        System.out.println("Enter array of integers with \",\" delimiter");
        System.out.println("Enter key, that will launch sorting or search. Keys:\n" +
                "b - Bubble sort\n" +
                "i - Insertion sort\n" +
                "s - Selection sort\n" +
                "q - Quick sort\n" +
                "f - Binary searsh");
    }
}

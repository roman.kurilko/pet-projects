package com.kurilko.algoritms;


public class BubbleSort {
    public static void sort(int[] ints, boolean direction) {
        boolean flag = true;
        while (flag) {
            flag = false;
            for (int j = 0; j < ints.length - 1; j++) {
                if (direction) {
                    if (ints[j] > ints[j + 1]) {
                        TextManipulations.swap(ints, j, j + 1);
                        flag = true;
                    }
                } else {
                    if (ints[j] < ints[j + 1]) {
                        TextManipulations.swap(ints, j + 1, j);
                        flag = true;
                    }
                }
            }
        }
    }
}

package com.kurilko.algoritms;

import java.util.List;

public class BinarySearch {

    protected <E extends Comparable<? super E>> int search(List<E> list, E element) {
        if (list.size() == 0)
            return -1;
        if (list.get(list.size() - 1).compareTo(element) < 0 || list.get(0).compareTo(element) > 0)
            return -1;
        int first = 1;
        int last = list.size();
        int position = first + (last - first) / 2;
        while (!list.get(position - 1).equals(element) && first <= last) {
            if (list.get(position).compareTo(element) > 0) {
                last = position - 1;
            } else {
                first = position + 1;
            }
            position = first + (last - first) / 2;
        }
        if (first <= last) {
            return position;
        } else {
            return -1;
        }
    }
}
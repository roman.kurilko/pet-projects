package com.kurilko.algoritms;


import org.junit.Assert;
import org.junit.Ignore;
import org.junit.Test;


public class QuickSortTest {

    @Test
    public void simpleSortTest() {
        int[] ints = {5, 7, 1, 2, 8, 6};
        int[] result = {1, 2, 5, 6, 7, 8};

        QuickSort.sort(ints, true);

        Assert.assertArrayEquals(ints, result);
    }

    @Ignore
    @Test
    public void bigArrayTest() {
        int minValue = 0;
        int maxValue = 214748000;
        int[] ints = new int[maxValue];
        for (int i = 0; i < ints.length; i++) {
            ints[i] = minValue + (int) (Math.random() * maxValue);
        }

        QuickSort.sort(ints, true);

        boolean flag = true;
        for (int i = 0; i < ints.length - 1; i++) {
            if (ints[i] > ints[i + 1]) {
                flag = false;
                break;
            }
        }
        Assert.assertTrue(flag);
    }

    @Test
    public void oneElementArrayTest() {
        int[] ints = {1};
        int[] result = {1};

        QuickSort.sort(ints, true);

        Assert.assertArrayEquals(ints, result);
    }

    @Test
    public void sameElementsArrayTest() {
        int[] ints = {1, 1, 1, 1, 1, 1, 1, 1, 1, 1};
        int[] result = {1, 1, 1, 1, 1, 1, 1, 1, 1, 1};

        QuickSort.sort(ints, true);

        Assert.assertArrayEquals(ints, result);
    }

    @Test
    public void reverseOrderTest() {
        int[] ints = {1, 2, 4, 3, 5, 6};
        int[] result = {6, 5, 4, 3, 2, 1};

        QuickSort.sort(ints, false);

        Assert.assertArrayEquals(ints, result);
    }
}

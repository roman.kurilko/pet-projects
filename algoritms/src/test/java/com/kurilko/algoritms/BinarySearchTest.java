package com.kurilko.algoritms;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

@RunWith(JUnit4.class)
public class BinarySearchTest {

    @Test
    public void oneElementTest() {
        List<Integer> ints = Collections.singletonList(1);

        BinarySearch bSearch = new BinarySearch();
        int result = bSearch.search(ints, 1);

        Assert.assertEquals(1, result);
    }

    @Test
    public void twoElementsTest() {
        List<Integer> ints = Arrays.asList(1, 2);

        BinarySearch bSearch = new BinarySearch();
        int result = bSearch.search(ints, 2);

        Assert.assertEquals(2, result);
    }

    @Test
    public void zeroElementsTest() {
        List<Integer> ints = new ArrayList<>();

        BinarySearch bSearch = new BinarySearch();
        int result = bSearch.search(ints, 2);

        Assert.assertEquals(-1, result);
    }

    @Test
    public void firstElementTest() {
        List<Integer> ints = new ArrayList<>();
        ints.add(1);
        ints.add(2);
        ints.add(3);
        ints.add(4);
        ints.add(5);
        ints.add(6);
        ints.add(7);

        BinarySearch bSearch = new BinarySearch();
        int result = bSearch.search(ints, 1);

        Assert.assertEquals(1, result);
    }

    @Test
    public void lastElementTest() {
        List<Integer> ints = new ArrayList<>();
        ints.add(1);
        ints.add(2);
        ints.add(3);
        ints.add(4);
        ints.add(5);
        ints.add(6);
        ints.add(7);

        BinarySearch bSearch = new BinarySearch();
        int result = bSearch.search(ints, 7);

        Assert.assertEquals(7, result);
    }

    @Test
    public void noElementTest() {
        List<Integer> ints = new ArrayList<>();
        ints.add(1);
        ints.add(2);
        ints.add(3);
        ints.add(4);
        ints.add(5);
        ints.add(6);
        ints.add(7);

        BinarySearch bSearch = new BinarySearch();
        int result = bSearch.search(ints, 12);

        Assert.assertEquals(-1, result);
    }

    @Test
    public void repeatingElementTest() {
        List<Integer> ints = new ArrayList<>();
        ints.add(1);
        ints.add(2);
        ints.add(3);
        ints.add(4);
        ints.add(4);
        ints.add(6);
        ints.add(7);

        BinarySearch bSearch = new BinarySearch();
        int result = bSearch.search(ints, 4);

        Assert.assertEquals(4, result);
    }

    @Test
    public void overRangeElementTest() {
        List<Integer> ints = new ArrayList<>();
        ints.add(1);
        ints.add(2);
        ints.add(3);
        ints.add(4);
        ints.add(5);
        ints.add(6);
        ints.add(7);

        BinarySearch bSearch = new BinarySearch();
        int result = bSearch.search(ints, 9);

        Assert.assertEquals(-1, result);
    }

    @Test
    public void underRangeElementTest() {
        List<Integer> ints = new ArrayList<>();
        ints.add(3);
        ints.add(4);
        ints.add(5);
        ints.add(6);
        ints.add(7);

        BinarySearch bSearch = new BinarySearch();
        int result = bSearch.search(ints, 1);

        Assert.assertEquals(-1, result);
    }
}

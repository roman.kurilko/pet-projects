package com.kurilko.dao.impl;

import com.kurilko.dao.ProductDao;
import com.kurilko.domain.Product;
import org.h2.tools.Server;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import static org.hamcrest.Matchers.hasItems;

public class ProductDBDaoImplTest {
    private static Server server;
    private static ProductDao productDao;
    private final static String SELECT_ONE_PRODUCT = "select \n" +
            "ID," +
            "NAME," +
            "PRICE " +
            "FROM PRODUCT " +
            "WHERE ID=?";
    ;
    private final static String DELETE_PRODUCT = "DELETE FROM PRODUCT WHERE ID=?";

    @Before
    public void setUp() {
        try {
            server = Server.createTcpServer("-tcpAllowOthers").start();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        productDao = new ProductDBDaoImpl();
    }

    @After
    public void tearDown() {
        productDao = null;
        server.stop();
    }

    @Test
    public void saveProductTest() {
        //GIVEN
        Product givenProduct = new Product(null, "test", new BigDecimal("1.1"));
        Product expectedProduct = givenProduct;
        //WHEN
        Product product = productDao.saveProduct(givenProduct);
        Product productFromDB = getProductFromDb(product.getId());
        expectedProduct.setId(product.getId());
        deleteClientFromDb(product.getId());
        //THEN
        Assert.assertEquals(expectedProduct, product);
        Assert.assertEquals(expectedProduct, productFromDB);
    }

    @Test
    public void getProductTest() {
        //GIVEN
        Product givenProduct = new Product(null, "test", new BigDecimal("1.1"));
        Product expectedProduct = givenProduct;
        //WHEN
        Product savedProduct = productDao.saveProduct(givenProduct);
        Product product = productDao.getProduct(savedProduct.getId());
        Product productFromDB = getProductFromDb(savedProduct.getId());
        expectedProduct.setId(savedProduct.getId());
        deleteClientFromDb(savedProduct.getId());
        //THEN
        Assert.assertEquals(expectedProduct, product);
        Assert.assertEquals(expectedProduct, productFromDB);
    }

    @Test
    public void getAllProductsTest() {
        //GIVEN
        Product givenProduct = new Product(null, "test", new BigDecimal("1.1"));
        Product expectedProduct = givenProduct;
        //WHEN
        Product savedProduct = productDao.saveProduct(givenProduct);
        List<Product> products = productDao.getAllProducts();
        Product productFromDB = getProductFromDb(savedProduct.getId());
        expectedProduct.setId(savedProduct.getId());
        deleteClientFromDb(savedProduct.getId());
        //THEN
        Assert.assertThat(products, hasItems(expectedProduct));
        Assert.assertThat(products, hasItems(productFromDB));
        Assert.assertEquals(expectedProduct, productFromDB);
    }

    @Test
    public void getProductsTest() {
        //GIVEN
        Product givenProduct = new Product(null, "test", new BigDecimal("1.1"));
        Product expectedProduct = givenProduct;
        ArrayList<Long> ids = new ArrayList<>();
        //WHEN
        Product savedProduct = productDao.saveProduct(givenProduct);
        ids.add(savedProduct.getId());
        List<Product> products = productDao.getProducts(ids);
        Product productFromDB = getProductFromDb(savedProduct.getId());
        expectedProduct.setId(savedProduct.getId());
        deleteClientFromDb(savedProduct.getId());
        //THEN
        Assert.assertThat(products, hasItems(expectedProduct));
        Assert.assertThat(products, hasItems(productFromDB));
        Assert.assertEquals(expectedProduct, productFromDB);
    }

    @Test
    public void updateProductTest() {
        //GIVEN
        Product givenProduct = new Product(null, "test", new BigDecimal("1.1"));
        Product changedProduct = new Product(null, "test2", new BigDecimal("2.2"));
        Product expectedProduct = changedProduct;
        //WHEN
        Product savedProduct = productDao.saveProduct(givenProduct);
        changedProduct.setId(savedProduct.getId());
        Product product = productDao.updateProduct(changedProduct);
        Product productFromDB = getProductFromDb(savedProduct.getId());
        expectedProduct.setId(savedProduct.getId());
        deleteClientFromDb(savedProduct.getId());
        //THEN
        Assert.assertEquals(expectedProduct, product);
        Assert.assertEquals(expectedProduct, productFromDB);
    }

    @Test
    public void deleteProductTest() {
        //GIVEN
        Product givenProduct = new Product(null, "test", new BigDecimal("1.1"));
        Product expectedProduct = givenProduct;
        //WHEN
        Product savedProduct = productDao.saveProduct(givenProduct);
        Product product = productDao.deleteProduct(savedProduct.getId());
        Product productFromDB = getProductFromDb(savedProduct.getId());
        expectedProduct.setId(savedProduct.getId());
        deleteClientFromDb(savedProduct.getId());
        //THEN
        Assert.assertEquals(expectedProduct, product);
        Assert.assertEquals(productFromDB.getId(), null);
    }

    private Product getProductFromDb(Long id) {
        Product result = new Product();
        try (Connection connection = new ConnectionManager().getConnection();
             PreparedStatement statement = connection.prepareStatement(SELECT_ONE_PRODUCT)) {
            statement.setLong(1, id);
            ResultSet resultSet = statement.executeQuery();
            while (resultSet.next()) {
                result = new Product(resultSet.getLong(1),
                        resultSet.getString(2),
                        resultSet.getBigDecimal(3));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return result;
    }

    public void deleteClientFromDb(Long id) {
        Product result = getProductFromDb(id);
        if (result == null)
            return;
        try (Connection connection = new ConnectionManager().getConnection();
             PreparedStatement statement = connection.prepareStatement(DELETE_PRODUCT)) {
            statement.setLong(1, id);
            int i = statement.executeUpdate();
            if (i < 1)
                return;
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
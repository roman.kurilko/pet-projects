package com.kurilko.dao.impl;

import com.kurilko.dao.ProductDao;
import com.kurilko.domain.Product;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import java.math.BigDecimal;
import java.util.List;

@RunWith(JUnit4.class)
public class ProductDaoImplTest {
    @Test
    public void getProductsTest() {
        ProductDao productDao = ProductDaoImpl.getInstance();
        productDao.saveProduct(new Product("First", new BigDecimal(1.1)));
        productDao.saveProduct(new Product("Second", new BigDecimal(2.2)));

        List<Product> products = productDao.getAllProducts();

        Assert.assertEquals(2, products.size());
    }
}

package com.kurilko.dao.impl;

import com.kurilko.dao.ClientDao;
import com.kurilko.dao.OrderDao;
import com.kurilko.dao.ProductDao;
import com.kurilko.domain.Client;
import com.kurilko.domain.Order;
import com.kurilko.domain.Product;
import org.h2.tools.Server;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static org.hamcrest.Matchers.hasItems;


public class OrderDBDaoImplTest {
    private OrderDao orderDao;
    private ClientDao clientDao;
    private ProductDao productDao;
    private static Server server;
    private final static String SELECT_ONE_ORDER = "select \n" +
            "ID, " +
            "CLIENT, " +
            "ORDERDATE " +
            "FROM ORDERS " +
            "WHERE ID=?";
    ;
    private final static String SELECT_ORDER_PRODUCTS = "select \n" +
            "ID, " +
            "ORDERID, " +
            "PRODUCTID " +
            "FROM PRODUCTS_IN_ORDERS " +
            "WHERE ORDERID=?";
    ;
    private final static String DELETE_ORDER = "DELETE FROM ORDERS WHERE ID=?";
    ;
    private final static String DELETE_PRODUCTS_FROM_ORDER = "DELETE FROM PRODUCTS_IN_ORDERS WHERE ORDERID=?";

    @Before
    public void setUp() {
        try {
            server = Server.createTcpServer("-tcpAllowOthers").start();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        orderDao = new OrderDBDaoImpl();
        clientDao = new ClientDBDaoImpl();
        productDao = new ProductDBDaoImpl();
    }

    @After
    public void tearDown() {
        orderDao = null;
        clientDao = null;
        productDao = null;
        server.stop();
    }

    @Test
    public void saveOrderTest() {
        //GIVEN
        Order givenOrder = makeTestOrder(null);
        Order expectedOrder = givenOrder;
        //WHEN
        Client client = clientDao.saveClient(givenOrder.getClient());
        Product product = productDao.saveProduct(givenOrder.getProducts().get(0));
        Order order = orderDao.saveOrder(givenOrder);
        Order orderFromDB = getOrderFromDb(order.getId());
        expectedOrder.setId(order.getId());
        deleteOrderFromDb(order.getId());
        productDao.deleteProduct(product.getId());
        clientDao.deleteClient(client.getId());
        //THEN
        Assert.assertEquals(expectedOrder, order);
        Assert.assertEquals(expectedOrder, orderFromDB);
    }

    @Test
    public void getOrderTest() {
        //GIVEN
        Order givenOrder = makeTestOrder(null);
        Order expectedOrder = givenOrder;
        //WHEN
        Client client = clientDao.saveClient(givenOrder.getClient());
        Product product = productDao.saveProduct(givenOrder.getProducts().get(0));
        Order savedOrder = orderDao.saveOrder(givenOrder);
        Order order = orderDao.getOrder(savedOrder.getId());
        Order orderFromDB = getOrderFromDb(savedOrder.getId());
        expectedOrder.setId(savedOrder.getId());
        deleteOrderFromDb(savedOrder.getId());
        productDao.deleteProduct(product.getId());
        clientDao.deleteClient(client.getId());
        //THEN
        Assert.assertEquals(expectedOrder, order);
        Assert.assertEquals(expectedOrder, orderFromDB);
    }

    @Test
    public void getAllOrdersTest() {
        //GIVEN
        Order givenOrder = makeTestOrder(null);
        Order expectedOrder = givenOrder;
        //WHEN
        Client client = clientDao.saveClient(givenOrder.getClient());
        Product product = productDao.saveProduct(givenOrder.getProducts().get(0));
        Order savedOrder = orderDao.saveOrder(givenOrder);
        List<Order> orders = orderDao.getAllOrders();
        Order orderFromDB = getOrderFromDb(savedOrder.getId());
        expectedOrder.setId(savedOrder.getId());
        deleteOrderFromDb(savedOrder.getId());
        productDao.deleteProduct(product.getId());
        clientDao.deleteClient(client.getId());
        //THEN
        Assert.assertThat(orders, hasItems(expectedOrder));
        Assert.assertThat(orders, hasItems(orderFromDB));
        Assert.assertEquals(expectedOrder, orderFromDB);
    }

    @Test
    public void getOrdersTest() {
        //GIVEN
        Order givenOrder = makeTestOrder(null);
        Order expectedOrder = givenOrder;
        //WHEN
        Client client = clientDao.saveClient(givenOrder.getClient());
        Product product = productDao.saveProduct(givenOrder.getProducts().get(0));
        Order savedOrder = orderDao.saveOrder(givenOrder);
        List<Order> orders = orderDao.getOrders(givenOrder.getClient().getId());
        Order orderFromDB = getOrderFromDb(savedOrder.getId());
        expectedOrder.setId(savedOrder.getId());
        deleteOrderFromDb(savedOrder.getId());
        productDao.deleteProduct(product.getId());
        clientDao.deleteClient(client.getId());
        //THEN
        Assert.assertThat(orders, hasItems(expectedOrder));
        Assert.assertThat(orders, hasItems(orderFromDB));
        Assert.assertEquals(expectedOrder, orderFromDB);
    }

    @Test
    public void updateOrderTest() {
        //GIVEN
        Order givenOrder = makeTestOrder(null);
        Long firstId = 1L;
        Client client = new Client(firstId, "1", "1", 0, "3333333333", "");
        ArrayList<Product> products = new ArrayList<>();
        products.add(new Product(firstId, "test2", new BigDecimal("2.2")));
        Date date = new Date();
        Order changedGiven = new Order(null, client, products, date);
        Order expectedOrder = changedGiven;
        //WHEN
        Client savedClient = clientDao.saveClient(givenOrder.getClient());
        Product product = productDao.saveProduct(givenOrder.getProducts().get(0));
        Product productChanged = productDao.saveProduct(changedGiven.getProducts().get(0));
        Order savedOrder = orderDao.saveOrder(givenOrder);
        changedGiven.setId(savedOrder.getId());
        Order order = orderDao.updateOrder(changedGiven);
        Order orderFromDB = getOrderFromDb(savedOrder.getId());
        expectedOrder.setId(savedOrder.getId());
        expectedOrder.setClient(savedClient);
        deleteOrderFromDb(savedOrder.getId());
        productDao.deleteProduct(product.getId());
        productDao.deleteProduct(productChanged.getId());
        clientDao.deleteClient(savedClient.getId());
        //THEN
        Assert.assertEquals(expectedOrder, order);
        Assert.assertEquals(expectedOrder, orderFromDB);
    }

    @Test
    public void deleteOrderTest() {
        //GIVEN
        Order givenOrder = makeTestOrder(null);
        Order expectedOrder = givenOrder;
        //WHEN
        Client savedClient = clientDao.saveClient(givenOrder.getClient());
        Product product = productDao.saveProduct(givenOrder.getProducts().get(0));
        Order savedOrder = orderDao.saveOrder(givenOrder);
        Order orderFromDB = getOrderFromDb(savedOrder.getId());
        Order order = orderDao.deleteOrder(savedOrder.getId());
        orderFromDB = getOrderFromDb(savedOrder.getId());
        expectedOrder.setId(savedOrder.getId());
        deleteOrderFromDb(savedOrder.getId());
        productDao.deleteProduct(product.getId());
        clientDao.deleteClient(savedClient.getId());
        //THEN
        Assert.assertEquals(expectedOrder, order);
        Assert.assertEquals(orderFromDB.getId(), null);
    }

    private Order getOrderFromDb(Long id) {
        Order result = new Order();
        try (Connection connection = new ConnectionManager().getConnection();
             PreparedStatement statement = connection.prepareStatement(SELECT_ONE_ORDER)) {
            statement.setLong(1, id);
            ResultSet resultSet = statement.executeQuery();
            while (resultSet.next()) {
                result = new Order(resultSet.getLong(1),
                        clientDao.getClient(resultSet.getLong(2)),
                        new ArrayList<Product>(),
                        resultSet.getDate(3));
            }
        } catch (SQLException e) {
            e.printStackTrace();
            System.out.println("Order doesn't exist!");
        }
        ArrayList<Product> orderProducts = getProductsFromOrder(id);
        result.setProducts(orderProducts);
        return result;
    }

    public void deleteOrderFromDb(Long id) {
        Order result = getOrderFromDb(id);
        if (result == null)
            return;
        deleteProductsFromOrder(id);
        try (Connection connection = new ConnectionManager().getConnection();
             PreparedStatement statement = connection.prepareStatement(DELETE_ORDER)) {
            statement.setLong(1, id);
            int i = statement.executeUpdate();
            if (i < 1)
                return;
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    private ArrayList<Product> getProductsFromOrder(Long id) {
        ArrayList<Product> orderProducts = new ArrayList<>();
        try (Connection connection = new ConnectionManager().getConnection();
             PreparedStatement statement = connection.prepareStatement(SELECT_ORDER_PRODUCTS)) {
            statement.setLong(1, id);
            ResultSet resultSet = statement.executeQuery();
            while (resultSet.next()) {
                orderProducts.add(productDao.getProduct(resultSet.getLong(3)));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return orderProducts;
    }

    private void deleteProductsFromOrder(Long id) {
        try (Connection connection = new ConnectionManager().getConnection();
             PreparedStatement statement = connection.prepareStatement(DELETE_PRODUCTS_FROM_ORDER)) {
            statement.setLong(1, id);
            statement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    private Order makeTestOrder(Long id) {
        Long firstId = 1L;
        Client client = new Client(firstId, "1", "1", 0, "4444444444", "");
        ArrayList<Product> products = new ArrayList<>();
        products.add(new Product(firstId, "test1", new BigDecimal("1.1")));
        Date date = new Date();
        return new Order(id, client, products, date);
    }
}
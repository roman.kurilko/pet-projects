package com.kurilko.dao.impl;

import com.kurilko.dao.ClientDao;
import com.kurilko.domain.Client;
import org.h2.tools.Server;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import static org.hamcrest.Matchers.hasItems;

@RunWith(JUnit4.class)
public class ClientDBDaoImplTest {

    private ClientDao clientDao;
    private static Server server;
    private static final String SELECT_ONE_CLIENT = "select \n" +
            "ID," +
            "NAME," +
            "SURNAME," +
            "AGE," +
            "PHONE," +
            "EMAIL " +
            "FROM client " +
            "WHERE ID=?";
    private final static String DELETE_CLIENT = "DELETE FROM CLIENT WHERE ID=?";

    @Before
    public void setUp() {
        try {
            server = Server.createTcpServer("-tcpAllowOthers").start();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        clientDao = new ClientDBDaoImpl();
    }

    @After
    public void tearDown() {
        clientDao = null;
        server.stop();
    }

    @Test
    public void saveClient() {
        //GIVEN
        Client givenClient = new Client(null, "1", "1", 0, "1111111111", "");
        Client expectedClient = new Client(null, "1", "1", 0, "1111111111", "");
        //WHEN
        Client client = clientDao.saveClient(givenClient);
        Client clientFromDB = getClientFromDb(client.getId());
        expectedClient.setId(client.getId());
        deleteClientFromDb(client.getId());
        //THEN
        Assert.assertEquals(expectedClient, client);
        Assert.assertEquals(expectedClient, clientFromDB);
    }

    @Test
    public void getClient() {
        //GIVEN
        Client givenClient = new Client(null, "1", "1", 0, "1111111111", "");
        Client expectedClient = new Client(null, "1", "1", 0, "1111111111", "");
        //WHEN
        Client savedClient = clientDao.saveClient(givenClient);
        Client client = clientDao.getClient(savedClient.getId());
        Client clientFromDB = getClientFromDb(savedClient.getId());
        expectedClient.setId(savedClient.getId());
        deleteClientFromDb(savedClient.getId());
        //THEN
        Assert.assertEquals(expectedClient, client);
        Assert.assertEquals(expectedClient, clientFromDB);
    }

    @Test
    public void getAllClients() {
        //GIVEN
        Client givenClient = new Client(null, "1", "1", 0, "1111111111", "");
        Client expectedClient = new Client(null, "1", "1", 0, "1111111111", "");
        //WHEN
        Client savedClient = clientDao.saveClient(givenClient);
        List<Client> clients = clientDao.getAllClients();
        Client clientFromDB = getClientFromDb(savedClient.getId());
        expectedClient.setId(savedClient.getId());
        deleteClientFromDb(savedClient.getId());
        //THEN
        Assert.assertThat(clients, hasItems(expectedClient));
        Assert.assertThat(clients, hasItems(clientFromDB));
        Assert.assertEquals(expectedClient, clientFromDB);
    }

    @Test
    public void updateClient() {
        //GIVEN
        Client givenClient = new Client(null, "1", "1", 0, "1111111111", "");
        Client changedClient = new Client(null, "2", "2", 30, "2222222222", "test@test.ru");
        Client expectedClient = new Client(null, "2", "2", 30, "2222222222", "test@test.ru");
        //WHEN
        Client savedClient = clientDao.saveClient(givenClient);
        expectedClient.setId(savedClient.getId());
        changedClient.setId(savedClient.getId());
        Client client = clientDao.updateClient(changedClient);
        Client clientFromDB = getClientFromDb(savedClient.getId());
        deleteClientFromDb(savedClient.getId());
        //THEN
        Assert.assertEquals(expectedClient, client);
        Assert.assertEquals(expectedClient, clientFromDB);
    }

    @Test
    public void deleteClient() {
        //GIVEN
        Client givenClient = new Client(null, "1", "1", 0, "1111111111", "");
        Client expectedClient = new Client(null, "1", "1", 0, "1111111111", "");
        //WHEN
        Client savedClient = clientDao.saveClient(givenClient);
        Client clientFromDB = getClientFromDb(savedClient.getId());
        Client client = clientDao.deleteClient(savedClient.getId());
        clientFromDB = getClientFromDb(savedClient.getId());
        expectedClient.setId(savedClient.getId());
        deleteClientFromDb(savedClient.getId());
        //THEN
        Assert.assertEquals(expectedClient, client);
        Assert.assertEquals(clientFromDB.getId(), null);
    }

    private Client getClientFromDb(Long id) {
        Client result = new Client();
        try (Connection connection = new ConnectionManager().getConnection();
             PreparedStatement statement = connection.prepareStatement(SELECT_ONE_CLIENT)) {
            statement.setLong(1, id);
            ResultSet resultSet = statement.executeQuery();
            while (resultSet.next()) {
                result = new Client(resultSet.getLong(1),
                        resultSet.getString(2),
                        resultSet.getString(3),
                        resultSet.getInt(4),
                        resultSet.getString(5),
                        resultSet.getString(6));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return result;
    }

    public void deleteClientFromDb(Long id) {
        Client result = getClientFromDb(id);
        if (result == null)
            return;
        try (Connection connection = new ConnectionManager().getConnection();
             PreparedStatement statement = connection.prepareStatement(DELETE_CLIENT)) {
            statement.setLong(1, id);
            int i = statement.executeUpdate();
            if (i < 1)
                return;
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
package com.kurilko.dao.impl;

import org.h2.tools.Server;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import java.sql.Connection;
import java.sql.Statement;
import java.sql.ResultSet;
import java.sql.SQLException;

@RunWith(JUnit4.class)
public class ConnectionManagerTest {
    private static Server server;

    @Before
    public void init() {
        try {
            server = Server.createTcpServer("-tcpAllowOthers").start();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @After
    public void tearDown() {
        server.stop();
    }

    @Test
    public void getConnection() {

        int result = 0;
        int expected = 2;
        try (Connection connection = new ConnectionManager().getConnection();
             Statement statement = connection.createStatement()) {
            ResultSet resultSet = statement.executeQuery("SELECT 2");
            resultSet.next();
            result = resultSet.getInt(1);
        } catch (SQLException e) {
            e.printStackTrace();
        }

        Assert.assertEquals(expected, result);
    }
}
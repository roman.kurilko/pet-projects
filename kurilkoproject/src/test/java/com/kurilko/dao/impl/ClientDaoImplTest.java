package com.kurilko.dao.impl;

import org.junit.Assert;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;

@RunWith(JUnit4.class)
public class ClientDaoImplTest {
    @Test
    public void existingTwoInstancesTest() {
        ClientDaoImpl one = ClientDaoImpl.getInstance();
        ClientDaoImpl two = ClientDaoImpl.getInstance();

        Assert.assertEquals(one, two);
    }

    @Ignore
    @Test
    public void existingTwoInstancesThroughReflectionTest() {
        ClientDaoImpl firstInstance = null;
        ClientDaoImpl secondInstance = null;
        try {
            Constructor<ClientDaoImpl> constructor = ClientDaoImpl.class.getDeclaredConstructor();
            constructor.setAccessible(true);
            firstInstance = constructor.newInstance();
            secondInstance = constructor.newInstance();
        } catch (InstantiationException | IllegalAccessException | InvocationTargetException | NoSuchMethodException e) {
            e.printStackTrace();
        }

        Assert.assertEquals(firstInstance, secondInstance);
    }
}

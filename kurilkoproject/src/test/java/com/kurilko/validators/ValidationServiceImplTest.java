package com.kurilko.validators;

import com.kurilko.dao.ClientDao;
import com.kurilko.dao.impl.ClientDaoImpl;
import com.kurilko.domain.Client;
import com.kurilko.exceptions.BusinessException;
import com.kurilko.services.ClientService;
import com.kurilko.services.impl.ClientServiceImpl;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;


@RunWith(JUnit4.class)
public class ValidationServiceImplTest {

    @Test
    public void businessExceptionTest() {
        String phone = "09778945611";
        ClientDao clientDao = ClientDaoImpl.getInstance();
        ValidationService validationService = new ValidationServiceImpl(clientDao);
        try {
            validationService.validatePhoneFormat(phone);
        } catch (BusinessException e) {
            Assert.assertEquals(BusinessException.class, e.getClass());
        }
    }

    @Test
    public void rightPhoneTest() {
        String[] phone = new String[4];
        phone[0] = "0977894561";
        phone[1] = "380977894561";
        phone[2] = "+38(097)999-99-99";
        phone[3] = "   +38 097 999 99 99    ";

        ClientDao clientDao = ClientDaoImpl.getInstance();
        ValidationService validationService = new ValidationServiceImpl(clientDao);
        try {
            for (int i = 0; i < phone.length; i++) {
                validationService.validatePhoneFormat(ClientServiceImpl.normalizePhoneNumber(phone[i]));
            }
        } catch (BusinessException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void charactersInPhoneTest() {
        String phone = "09ph894561";
        ClientDao clientDao = ClientDaoImpl.getInstance();
        ValidationService validationService = new ValidationServiceImpl(clientDao);
        try {
            validationService.validatePhoneFormat(phone);
        } catch (Exception e) {
            Assert.assertEquals(BusinessException.class, e.getClass());
        }
    }

    @Test
    public void uniquenessPhoneTest() throws BusinessException {
        ClientDao clientDao = ClientDaoImpl.getInstance();
        ValidationService validationService = new ValidationServiceImpl(clientDao);
        ClientService clientService = new ClientServiceImpl(clientDao, validationService);
        try {
            Client client = clientService.createClient(new Client("1", "1", "0979999999"));
            client = clientService.createClient(new Client("1", "1", "0979999999"));
        } catch (Exception e) {
            Assert.assertEquals(BusinessException.class, e.getClass());
        }
    }

    @Test
    public void correctEmailFormatTest() {
        String email = "exa.m-ple@gmail.com.ua";
        ClientDao clientDao = ClientDaoImpl.getInstance();
        ValidationService validationService = new ValidationServiceImpl(clientDao);
        try {
            validationService.validateEmail(email);
        } catch (BusinessException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void emailWithWrongCharactersTest() {
        String email = "exa%mple@gmail.com";
        ClientDao clientDao = ClientDaoImpl.getInstance();
        ValidationService validationService = new ValidationServiceImpl(clientDao);
        try {
            validationService.validateEmail(email);
        } catch (BusinessException e) {
            Assert.assertEquals(BusinessException.class, e.getClass());
        }
    }

    @Test
    public void emailWithoutAtSymbolTest() {
        String email = "exa%mplegmail.com";
        ClientDao clientDao = ClientDaoImpl.getInstance();
        ValidationService validationService = new ValidationServiceImpl(clientDao);
        try {
            validationService.validateEmail(email);
        } catch (BusinessException e) {
            Assert.assertEquals(BusinessException.class, e.getClass());
        }
    }

    @Test
    public void validateAgeTest() throws BusinessException {
        int age = 201;
        ClientDao clientDao = ClientDaoImpl.getInstance();
        ValidationService validationService = new ValidationServiceImpl(clientDao);
        try {
            validationService.validateAge(age);
        } catch (BusinessException e) {
            Assert.assertEquals(BusinessException.class, e.getClass());
        }

    }
}

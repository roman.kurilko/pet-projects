package com.kurilko.services.impl;

import com.kurilko.dao.OrderDao;
import com.kurilko.domain.Client;
import com.kurilko.domain.Order;
import com.kurilko.domain.Product;
import com.kurilko.services.OrderService;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@RunWith(MockitoJUnitRunner.class)
public class OrderServiceImplTest {
    @Mock
    private OrderDao orderDao;
    @Mock
    private OrderService orderService;

    @Before
    public void init() {
        orderService = new OrderServiceImpl(orderDao);
    }

    @Test
    public void createOrderTest() {
        //GIVEN
        Long firstId = 1L;
        Order givenOrder = makeTestOrder(null);
        Order expectedOrder = makeTestOrder(firstId);
        Mockito.when(orderDao.saveOrder(givenOrder)).thenReturn(expectedOrder);
        //WHEN
        Order order = orderService.createOrder(givenOrder);
        //THEN
        Mockito.verify(orderDao).saveOrder(givenOrder);
        Mockito.verifyNoMoreInteractions(orderDao);
        Assert.assertEquals(expectedOrder, order);
    }

    @Test
    public void readOrderTest() {
        //GIVEN
        Long firstId = 1L;
        Order expectedOrder = makeTestOrder(firstId);
        Mockito.when(orderDao.getOrder(firstId)).thenReturn(expectedOrder);
        //WHEN
        Order order = orderService.readOrder(firstId);
        //THEN
        Mockito.verify(orderDao).getOrder(firstId);
        Mockito.verifyNoMoreInteractions(orderDao);
        Assert.assertEquals(expectedOrder, order);
    }

    @Test
    public void getOrdersTest() {
        //GIVEN
        Long firstId = 1L;
        Long secondId = 2L;
        ArrayList<Order> expectedOrders = new ArrayList<>();
        expectedOrders.add(makeTestOrder(firstId));
        expectedOrders.add(makeTestOrder(secondId));
        Mockito.when(orderDao.getAllOrders()).thenReturn(expectedOrders);
        //WHEN
        List<Order> orders = orderService.getOrders(0,20);
        //THEN
        Mockito.verify(orderDao).getAllOrders();
        Mockito.verifyNoMoreInteractions(orderDao);
        Assert.assertEquals(expectedOrders, orders);
    }

    @Test
    public void getOrdersByIdTest() {
        //GIVEN
        Long firstId = 1L;
        Long secondId = 2L;
        ArrayList<Order> expectedOrders = new ArrayList<>();
        expectedOrders.add(makeTestOrder(firstId));
        expectedOrders.add(makeTestOrder(secondId));
        Mockito.when(orderDao.getOrders(firstId)).thenReturn(expectedOrders);
        //WHEN
        List<Order> orders = orderService.getOrdersById(firstId);
        //THEN
        Mockito.verify(orderDao).getOrders(firstId);
        Mockito.verifyNoMoreInteractions(orderDao);
        Assert.assertEquals(expectedOrders, orders);
    }

    @Test
    public void updateOrderTest() {
        //GIVEN
        Long firstId = 1L;
        Long secondId = 2L;
        Order givenOrder = makeTestOrder(firstId);
        Client client = new Client(secondId, "2", "2", 32, "2222222222", "test2@test.com");
        ArrayList<Product> products = new ArrayList<>();
        products.add(new Product(firstId, "test1", new BigDecimal(1.1)));
        Date date = new Date();
        Order expectedOrder =  new Order(firstId, client, products, date);
        Mockito.when(orderDao.updateOrder(givenOrder)).thenReturn(expectedOrder);
        //WHEN
        Order order = orderService.updateOrder(givenOrder);
        //THEN
        Mockito.verify(orderDao).updateOrder(givenOrder);
        Mockito.verifyNoMoreInteractions(orderDao);
        Assert.assertEquals(expectedOrder, order);
    }

    @Test
    public void deleteOrderTest() {
        //GIVEN
        Long firstId = 1L;
        Order expectedOrder =  makeTestOrder(firstId);
        Mockito.when(orderDao.deleteOrder(firstId)).thenReturn(expectedOrder);
        //WHEN
        Order order = orderService.deleteOrder(firstId);
        //THEN
        Mockito.verify(orderDao).deleteOrder(firstId);
        Mockito.verifyNoMoreInteractions(orderDao);
        Assert.assertEquals(expectedOrder, order);
    }

    @After
    public void teatDown() {
        orderService = null;
    }

    private Order makeTestOrder(Long id) {
        Long firstId = 1L;
        Long secondId = 2L;
        Client client = new Client(firstId, "1", "1", 30, "1111111111", "test@test.com");
        ArrayList<Product> products = new ArrayList<>();
        products.add(new Product(firstId, "test1", new BigDecimal(1.1)));
        products.add(new Product(secondId, "test2", new BigDecimal(2.2)));
        Date date = new Date();
        return new Order(id, client, products, date);
    }
}
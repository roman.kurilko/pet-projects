package com.kurilko.services.impl;

import org.junit.Assert;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;


public class ConversionTest {

    @Test
    public void stringArrayToLongTest() {
        //GIVEN
        String[] ids = new String[3];
        ids[0] = "1";
        ids[1] = "2";
        ids[2] = "3";
        ArrayList<Long> expectedList = new ArrayList<>();
        expectedList.add(1L);
        expectedList.add(2L);
        expectedList.add(3L);
        //WHEN
        List<Long> list = Conversion.stringArrayToLong(ids);
        //THEN
        Assert.assertEquals(expectedList, list);
    }
}
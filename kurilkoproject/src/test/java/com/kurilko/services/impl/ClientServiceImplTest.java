package com.kurilko.services.impl;

import com.kurilko.dao.ClientDao;
import com.kurilko.domain.Client;
import com.kurilko.services.ClientService;
import com.kurilko.validators.ValidationService;
import org.junit.*;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.ArrayList;
import java.util.List;

@RunWith(MockitoJUnitRunner.class)
public class ClientServiceImplTest {

    @Mock
    private ClientDao clientDao;
    @Mock
    private ValidationService validationService;
    private ClientService clientService;

    @Before
    public void init() {
        clientService = new ClientServiceImpl(clientDao, validationService);
    }

    @Test
    public void createClientTest() {
        //GIVEN
        long id = 1;
        Client givenClient = new Client(null, "1","1",30,"1111111111","test@test.com");
        Client expectedClient = new Client(id, "1","1",30,"1111111111","test@test.com");
        Mockito.when(clientDao.saveClient(givenClient)).thenReturn(expectedClient);
        //WHEN
        Client client = clientService.createClient(givenClient);
        //THEN
        Mockito.verify(clientDao).saveClient(givenClient);
        Mockito.verifyNoMoreInteractions(clientDao);
        Assert.assertEquals(expectedClient, client);
    }

    @Test
    public void readClientTest() {
        //GIVEN
        long id = 1;
        Client expectedClient = new Client(id, "1","1",30,"1111111111","test@test.com");
        Mockito.when(clientDao.getClient(id)).thenReturn(expectedClient);
        //WHEN
        Client client = clientService.readClient(id);
        //THEN
        Mockito.verify(clientDao).getClient(1L);
        Mockito.verifyNoMoreInteractions(clientDao);
        Assert.assertEquals(expectedClient, client);
    }

    @Test
    public void getAllClientsTest() {
        //GIVEN
        long firstId = 1;
        long secondId = 2;
        ArrayList<Client> expectedClients = new ArrayList<>();
        expectedClients.add(new Client(firstId, "1","1",30,"1111111111","test@test.com"));
        expectedClients.add(new Client(secondId, "2","2",32,"2222222222","test2@test2.com"));
        Mockito.when(clientDao.getAllClients()).thenReturn(expectedClients);
        //WHEN
        List<Client> returnedClients = clientDao.getAllClients();
        //THEN
        Mockito.verify(clientDao).getAllClients();
        Mockito.verifyNoMoreInteractions(clientDao);
        Assert.assertEquals(expectedClients, returnedClients);
    }

    @Test
    public void updateClientTest() {
        //GIVEN
        long id = 1;
        Client changingClient = new Client(id, "1","1",30,"1111111111","test@test.com");
        Client expectedClient = new Client(id, "2","2",32,"2222222222","test2@test2.com");
        Mockito.when(clientDao.updateClient(changingClient)).thenReturn(expectedClient);
        //WHEN
        Client client = clientService.updateClient(changingClient);
        //THEN
        Mockito.verify(clientDao).updateClient(changingClient);
        Mockito.verifyNoMoreInteractions(clientDao);
        Assert.assertEquals(expectedClient, client);
    }

    @Test
    public void deleteClientTest() {
        //GIVEN
        long id = 1;
        Client expectedClient = new Client(id, "1","1",30,"1111111111","test@test.com");
        Mockito.when(clientDao.deleteClient(id)).thenReturn(expectedClient);
        //WHEN
        Client client = clientService.deleteClient(id);
        //THEN
        Mockito.verify(clientDao).deleteClient(1L);
        Mockito.verifyNoMoreInteractions(clientDao);
        Assert.assertEquals(expectedClient, client);
    }

    @Test
    public void normalizePhoneNumberTest() {
        //GIVEN
        String givenPhone = "  +38(097) 99-999-99-99  ";
        String expectedPhone = "38097999999999";
        //WHEN
        String phone = ClientServiceImpl.normalizePhoneNumber(givenPhone);
        //THEN
        Assert.assertEquals(expectedPhone, phone);
    }

    @After
    public void teatDown() {
        clientService = null;
    }
}
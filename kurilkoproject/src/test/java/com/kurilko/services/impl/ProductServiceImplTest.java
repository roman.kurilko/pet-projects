package com.kurilko.services.impl;

import com.kurilko.dao.ProductDao;
import com.kurilko.domain.Product;
import com.kurilko.services.ProductService;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;


@RunWith(MockitoJUnitRunner.class)
public class ProductServiceImplTest {

    @Mock
    private ProductDao productDao;
    @Mock
    private ProductService productService;

    @Before
    public void init() {
        productService = new ProductServiceImpl(productDao);
    }
    @Test
    public void createProductTest() {
        //GIVEN
        Long firstId = 1L;
        Product givenProduct = new Product(null, "testProduct", new BigDecimal(1.1));
        Product expectedProduct = new Product(firstId, "testProduct", new BigDecimal(1.1));
        Mockito.when(productDao.saveProduct(givenProduct)).thenReturn(expectedProduct);
        //WHEN
        Product product = productService.createProduct(givenProduct);
        //THEN
        Mockito.verify(productDao).saveProduct(givenProduct);
        Mockito.verifyNoMoreInteractions(productDao);
        Assert.assertEquals(expectedProduct, product);
    }

    @Test
    public void readProductTest() {
        //GIVEN
        Long firstId = 1L;
        Product expectedProduct = new Product(firstId, "testProduct", new BigDecimal(1.1));
        Mockito.when(productDao.getProduct(firstId)).thenReturn(expectedProduct);
        //WHEN
        Product product = productService.readProduct(firstId);
        //THEN
        Mockito.verify(productDao).getProduct(firstId);
        Mockito.verifyNoMoreInteractions(productDao);
        Assert.assertEquals(expectedProduct, product);
    }

    @Test
    public void getProductsTest() {
        //GIVEN
        Long firstId = 1L;
        Long secondId = 2L;
        ArrayList<Long> ids = new ArrayList<>();
        ids.add(firstId);
        ids.add(secondId);
        ArrayList<Product> expectedProducts = new ArrayList<>();
        expectedProducts.add(new Product(firstId, "testProduct", new BigDecimal(1.1)));
        expectedProducts.add(new Product(secondId, "testProduct2", new BigDecimal(2.2)));
        Mockito.when(productDao.getProducts(ids)).thenReturn(expectedProducts);
        //WHEN
        List<Product> products = productService.getProducts(ids);
        //THEN
        Mockito.verify(productDao).getProducts(ids);
        Mockito.verifyNoMoreInteractions(productDao);
        Assert.assertEquals(expectedProducts, products);
    }

    @Test
    public void getProducts1Test() {
        //GIVEN
        Long firstId = 1L;
        Long secondId = 2L;
        ArrayList<Long> ids = new ArrayList<>();
        ids.add(firstId);
        ids.add(secondId);
        ArrayList<Product> expectedProducts = new ArrayList<>();
        expectedProducts.add(new Product(firstId, "testProduct", new BigDecimal(1.1)));
        expectedProducts.add(new Product(secondId, "testProduct2", new BigDecimal(2.2)));
        Mockito.when(productDao.getAllProducts()).thenReturn(expectedProducts);
        //WHEN
        List<Product> products = productService.getProducts(0,20);
        //THEN
        Mockito.verify(productDao).getAllProducts();
        Mockito.verifyNoMoreInteractions(productDao);
        Assert.assertEquals(expectedProducts, products);
    }

    @Test
    public void updateProductTest() {
        //GIVEN
        Long firstId = 1L;
        Product givenProduct = new Product(firstId, "testProduct", new BigDecimal(1.1));
        Product expectedProduct = new Product(firstId, "product", new BigDecimal(2.2));
        Mockito.when(productDao.updateProduct(givenProduct)).thenReturn(expectedProduct);
        //WHEN
        Product product = productService.updateProduct(givenProduct);
        //THEN
        Mockito.verify(productDao).updateProduct(givenProduct);
        Mockito.verifyNoMoreInteractions(productDao);
        Assert.assertEquals(expectedProduct, product);
    }

    @Test
    public void deleteProductTest() {
        //GIVEN
        Long firstId = 1L;
        Product expectedProduct = new Product(firstId, "product", new BigDecimal(2.2));
        Mockito.when(productDao.deleteProduct(firstId)).thenReturn(expectedProduct);
        //WHEN
        Product product = productService.deleteProduct(firstId);
        //THEN
        Mockito.verify(productDao).deleteProduct(firstId);
        Mockito.verifyNoMoreInteractions(productDao);
        Assert.assertEquals(expectedProduct, product);
    }

    @After
    public void teatDown() {
        productService = null;
    }
}
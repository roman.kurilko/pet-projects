package com.kurilko.services.impl;

import com.kurilko.dao.ClientDao;
import com.kurilko.validators.ValidationService;
import com.kurilko.validators.ValidationServiceImpl;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import java.io.BufferedReader;
import java.io.IOException;
import java.math.BigDecimal;


@RunWith(MockitoJUnitRunner.class)
public class ReadDataTest {
    @Mock
    private BufferedReader bufferedReader;
    @Mock
    private ClientDao clientDao;
    private ValidationService validationService = new ValidationServiceImpl(clientDao);

    @Test
    public void readBigDecimalTest() throws IOException {
        //GIVEN
        String read = "1.1";
        BigDecimal expected = new BigDecimal(read);
        Mockito.when(bufferedReader.readLine()).thenReturn(read);
        //WHEN
        BigDecimal number = ReadData.readBigDecimal(bufferedReader);
        //THEN
        Assert.assertEquals(expected, number);
    }

    @Test
    public void readIntegerTest() throws IOException {
        //GIVEN
        String read = "2000000000";
        Integer expected = 2000000000;
        Mockito.when(bufferedReader.readLine()).thenReturn(read);
        //WHEN
        Integer number = ReadData.readInteger(bufferedReader);
        //THEN
        Assert.assertEquals(expected, number);
    }

    @Test
    public void readLongTest() throws IOException {
        //GIVEN
        String read = "3000000000";
        Long expected = 3000000000L;
        Mockito.when(bufferedReader.readLine()).thenReturn(read);
        //WHEN
        Long number = ReadData.readLong(bufferedReader);
        //THEN
        Assert.assertEquals(expected, number);
    }

    @Test
    public void readStringTest() throws IOException {
        //GIVEN
        String read = "string";
        String expected = "string";
        Mockito.when(bufferedReader.readLine()).thenReturn(read);
        //WHEN
        String number = ReadData.readString(bufferedReader);
        //THEN
        Assert.assertEquals(expected, number);
    }

    @Test
    public void readAgeTest() throws IOException {
        //GIVEN
        String read = "100";
        Integer expected = 100;
        Mockito.when(bufferedReader.readLine()).thenReturn(read);
        //WHEN
        Integer number = ReadData.readAge(bufferedReader, validationService);
        //THEN
        Assert.assertEquals(expected, number);
    }
}
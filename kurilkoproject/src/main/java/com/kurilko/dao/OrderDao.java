package com.kurilko.dao;

import com.kurilko.domain.Order;

import java.util.List;

public interface OrderDao {
    /**
     * Method saves new order in the storage
     *
     * @param order takes instance of Order to save it
     * @return true if saving was successful and false if there wasn't
     */
    Order saveOrder(Order order);

    /**
     * Method gets instance of Order by his id from the storage
     *
     * @param id is an id of order that we are getting
     * @return instance of Order
     */
    Order getOrder(Long id);

    /**
     * Method gets list of orders to show
     *
     * @return list of Orders
     */
    List<Order> getAllOrders();

    /**
     * Method gets list of orders to show by ID of client
     *
     * @param clientId ID of client to show orders
     * @return list of Orders
     */
    List<Order> getOrders(Long clientId);

    /**
     * Method updates existing instance of the Order by new instance, that is received from the parameter
     *
     * @param order new instance of the Order that will replace old one
     * @return replaced new instance of the Order
     */
    Order updateOrder(Order order);

    /**
     * Method deletes existing instance of the Order by id, that is received from the parameter
     *
     * @param id is an id of deleting instance from the storage
     * @return instance of deleted Order, that we can show after deleting operation
     */
    Order deleteOrder(Long id);
}

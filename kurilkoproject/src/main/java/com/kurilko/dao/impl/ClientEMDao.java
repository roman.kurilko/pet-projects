package com.kurilko.dao.impl;

import com.kurilko.dao.ClientDao;
import com.kurilko.domain.Client;
import org.springframework.beans.factory.annotation.Autowired;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import java.util.List;

public class ClientEMDao implements ClientDao{
    private EntityManager entityManager;

    @Autowired
    public ClientEMDao() {
        EntityManagerFactory factory = Persistence.createEntityManagerFactory("persistence-unit");
        this.entityManager = factory.createEntityManager();
    }

    @Override
    public Client saveClient(Client client) {
        entityManager.getTransaction().begin();
        entityManager.persist(client);
        entityManager.getTransaction().commit();
        return client;
    }

    @Override
    public Client getClient(Long id) {
        Client client = entityManager.createQuery("from Client Where id="+id, Client.class).getSingleResult();
        return client;
    }

    @Override
    public List<Client> getAllClients() {
        List<Client> clients = entityManager.createQuery("from Client", Client.class).getResultList();
        return clients;
    }

    @Override
    public Client updateClient(Client client) {
        entityManager.getTransaction().begin();
        entityManager.merge(client);
        entityManager.getTransaction().commit();
        return client;
    }

    @Override
    public Client deleteClient(Long id) {
        entityManager.getTransaction().begin();
        Client deletedClient = entityManager.find(Client.class, id);
        entityManager.remove(deletedClient);
        entityManager.getTransaction().commit();
        return deletedClient;
    }
}

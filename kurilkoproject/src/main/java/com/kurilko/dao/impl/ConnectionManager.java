package com.kurilko.dao.impl;

import com.kurilko.App;

import java.io.IOException;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Properties;

public class ConnectionManager {
    public Connection getConnection() {
        Properties property = new Properties();
        try (InputStream in =
                     App.class.getClassLoader().getResourceAsStream("db.properties")) {
            property.load(in);
        } catch (IOException e1) {
            e1.printStackTrace();
        }
        String drivers = property.getProperty("db.driver");
        if (drivers != null) {
            System.setProperty("db.driver", drivers);
        }

        String url = property.getProperty("db.url");
        String user = property.getProperty("db.user");
        String password = property.getProperty("db.password");

        Connection connection = null;
        try {
            connection = DriverManager.getConnection(url, user, password);
        } catch (SQLException e) {
            System.err.println("ОШИБКА: Не удалось подключиться к БД!");
            e.printStackTrace();
        }
        return connection;
    }
}

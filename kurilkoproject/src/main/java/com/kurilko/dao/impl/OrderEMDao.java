package com.kurilko.dao.impl;

import com.kurilko.dao.OrderDao;
import com.kurilko.domain.Order;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.TypedQuery;
import java.util.List;

public class OrderEMDao implements OrderDao {
    private EntityManager entityManager;

    public OrderEMDao() {
        EntityManagerFactory factory = Persistence.createEntityManagerFactory("persistence-unit");
        this.entityManager = factory.createEntityManager();
    }

    @Override
    public Order saveOrder(Order order) {
        entityManager.getTransaction().begin();
        entityManager.persist(order);
        entityManager.getTransaction().commit();
        return order;
    }

    @Override
    public Order getOrder(Long id) {
        Order order = entityManager.find(Order.class, id);
        return order;
    }

    @Override
    public List<Order> getAllOrders() {
        List<Order> orders = entityManager.createQuery("from Order", Order.class).getResultList();
        return orders;
    }

    @Override
    public List<Order> getOrders(Long clientId) {
        String queryString = "from Order where client = clientid";
        TypedQuery<Order> query = entityManager.createQuery(queryString, Order.class);
        query.setParameter("clientid", clientId);
        List<Order> orders = query.getResultList();
        return orders;
    }

    @Override
    public Order updateOrder(Order order) {
        entityManager.getTransaction().begin();
        entityManager.merge(order);
        entityManager.getTransaction().commit();
        return order;
    }

    @Override
    public Order deleteOrder(Long id) {
        entityManager.getTransaction().begin();
        Order deletedOrder = entityManager.find(Order.class, id);
        entityManager.remove(deletedOrder);
        entityManager.getTransaction().commit();
        return deletedOrder;
    }
}

package com.kurilko.dao.impl;

import com.kurilko.dao.ProductDao;
import com.kurilko.domain.Product;

import java.math.BigDecimal;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class ProductDBDaoImpl implements ProductDao {
    private final static String CREATE_TABLE_PRODUCT;
    private final static String INSERT_NEW_PRODUCT;
    private final static String SELECT_ALL_PRODUCTS;
    private final static String SELECT_ONE_PRODUCT;
    private final static String SELECT_LIST_OF_PRODUCTS;
    private final static String UPDATE_PRODUCT;
    private final static String DELETE_PRODUCT;

    static {
        CREATE_TABLE_PRODUCT = "CREATE TABLE IF NOT EXISTS PRODUCT \n" +
                "(ID BIGINT PRIMARY KEY AUTO_INCREMENT,\n" +
                "NAME VARCHAR(20),\n" +
                "PRICE FLOAT\n" +
                ");";
        INSERT_NEW_PRODUCT = "insert into PRODUCT (name,price) \n" +
                "values (?,?);";
        SELECT_ALL_PRODUCTS = "select \n" +
                "ID," +
                "NAME," +
                "PRICE " +
                "FROM PRODUCT";
        SELECT_ONE_PRODUCT = "select \n" +
                "ID," +
                "NAME," +
                "PRICE " +
                "FROM PRODUCT " +
                "WHERE ID=?";
        SELECT_LIST_OF_PRODUCTS = "select \n" +
                "ID," +
                "NAME," +
                "PRICE " +
                "FROM PRODUCT " +
                "WHERE ID IN (";
        UPDATE_PRODUCT = "UPDATE PRODUCT\n" +
                "SET NAME = ?,\n" +
                "PRICE = ?\n" +
                "WHERE ID=?";
        DELETE_PRODUCT = "DELETE FROM PRODUCT WHERE ID=?";
    }

    public ProductDBDaoImpl() {
        try (Connection connection = new ConnectionManager().getConnection();
             Statement statement = connection.createStatement()) {
            statement.execute(CREATE_TABLE_PRODUCT);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public Product saveProduct(Product product) {
        try (Connection connection = new ConnectionManager().getConnection();
             PreparedStatement statement = connection.prepareStatement(INSERT_NEW_PRODUCT, PreparedStatement.RETURN_GENERATED_KEYS)) {
            statement.setString(1, product.getName());
            statement.setBigDecimal(2, product.getPrice());
            statement.execute();
            ResultSet rs = statement.getGeneratedKeys();
            rs.next();
            product.setId(rs.getLong(1));
        } catch (SQLException e) {
            System.out.println("Product doesn't saved!");
        }
        return product;
    }

    @Override
    public Product getProduct(Long id) {
        Product result = null;
        try (Connection connection = new ConnectionManager().getConnection();
             PreparedStatement statement = connection.prepareStatement(SELECT_ONE_PRODUCT)) {
            statement.setLong(1, id);
            ResultSet resultSet = statement.executeQuery();
            while (resultSet.next()) {
                result = new Product(resultSet.getLong(1),
                        resultSet.getString(2),
                        resultSet.getBigDecimal(3));
            }
        } catch (SQLException e) {
            System.out.println("Product doesn't exist!");
        }
        return result;
    }

    @Override
    public List<Product> getAllProducts() {
        List<Product> result = new ArrayList<>();
        try (Connection connection = new ConnectionManager().getConnection();
             Statement statement = connection.createStatement()) {
            try (ResultSet resultSet = statement.executeQuery(SELECT_ALL_PRODUCTS)) {
                while (resultSet.next()) {
                    result.add(new Product(resultSet.getLong(1),
                            resultSet.getString(2),
                            resultSet.getBigDecimal(3)));
                }
            } catch (SQLException e) {
                System.out.println("There are no products!");
            }
        } catch (SQLException e) {
            System.out.println("Products don't find!");
        }
        return result;
    }

    @Override
    public List<Product> getProducts(List<Long> ids) {
        ArrayList<Product> result = new ArrayList<>();
        try (Connection connection = new ConnectionManager().getConnection()) {
            StringBuilder builder = new StringBuilder();
            for (Long id:ids) {
                builder.append("?,");
            }
            String stmt = SELECT_LIST_OF_PRODUCTS
                    + builder.deleteCharAt(builder.length() - 1).toString()+")";
            PreparedStatement statement = connection.prepareStatement(stmt);
            int index = 1;
            for (Object o : ids) {
                statement.setObject(index++, o);
            }
            ResultSet resultSet = statement.executeQuery();
            while (resultSet.next()) {
                Long id = resultSet.getLong(1);
                String name = resultSet.getString(2);
                BigDecimal price = new BigDecimal(resultSet.getString(3));
                result.add(new Product(id,
                        name,
                        price));
            }
        } catch (SQLException e) {
            e.printStackTrace();
            System.out.println("Products don't exist!");
        }
        return result;
    }

    @Override
    public Product updateProduct(Product product) {
        try (Connection connection = new ConnectionManager().getConnection();
             PreparedStatement statement = connection.prepareStatement(UPDATE_PRODUCT)) {
            statement.setString(1, product.getName());
            statement.setBigDecimal(2, product.getPrice());
            statement.setLong(3, product.getId());
            statement.execute();
        } catch (SQLException e) {
            e.printStackTrace();
            System.out.println("Product doesn't updated!");
        }
        return product;
    }

    @Override
    public Product deleteProduct(Long id) {
        Product result = getProduct(id);
        if (result == null)
            return result;
        try (Connection connection = new ConnectionManager().getConnection();
             PreparedStatement statement = connection.prepareStatement(DELETE_PRODUCT)) {
            statement.setLong(1, id);
            int i = statement.executeUpdate();
            if (i < 1)
                return result;
        } catch (SQLException e) {
            System.out.println("Product doesn't deleted!");
        }
        return result;
    }
}

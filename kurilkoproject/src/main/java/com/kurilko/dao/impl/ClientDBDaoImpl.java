package com.kurilko.dao.impl;

import com.kurilko.dao.ClientDao;
import com.kurilko.domain.Client;
import org.springframework.beans.factory.annotation.Autowired;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;


public class ClientDBDaoImpl implements ClientDao {
    private final static String CREATE_TABLE_CLIENT;
    private final static String INSERT_NEW_CLIENT;
    private final static String SELECT_ALL_CLIENTS;
    private final static String SELECT_ONE_CLIENT;
    private final static String UPDATE_CLIENT;
    private final static String DELETE_CLIENT;

    static {
        CREATE_TABLE_CLIENT = "create table if not exists CLIENT \n" +
                "(ID BIGINT PRIMARY KEY AUTO_INCREMENT,\n" +
                "NAME VARCHAR(20),\n" +
                "SURNAME VARCHAR(20),\n" +
                "AGE INT,\n" +
                "PHONE VARCHAR(20),\n" +
                "EMAIL VARCHAR(20)\n" +
                ");";
        INSERT_NEW_CLIENT = "insert into client (name,surname,age,phone,email) \n" +
                "values (?,?,?,?,?);";
        SELECT_ALL_CLIENTS = "select \n" +
                "ID," +
                "NAME," +
                "SURNAME," +
                "AGE," +
                "PHONE," +
                "EMAIL " +
                "FROM client";
        SELECT_ONE_CLIENT = "select \n" +
                "ID," +
                "NAME," +
                "SURNAME," +
                "AGE," +
                "PHONE," +
                "EMAIL " +
                "FROM client " +
                "WHERE ID=?";
        UPDATE_CLIENT = "UPDATE CLIENT\n" +
                "SET NAME = ?,\n" +
                "SURNAME = ?,\n" +
                "AGE = ?,\n" +
                "PHONE = ?,\n" +
                "EMAIL = ?\n" +
                "WHERE ID=?";
        DELETE_CLIENT = "DELETE FROM CLIENT WHERE ID=?";
    }

    @Autowired
    public ClientDBDaoImpl() {
        try (Connection connection = new ConnectionManager().getConnection();
             Statement statement = connection.createStatement()) {
            statement.execute(CREATE_TABLE_CLIENT);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public Client saveClient(Client client) {
        try (Connection connection = new ConnectionManager().getConnection();
             PreparedStatement statement = connection.prepareStatement(INSERT_NEW_CLIENT, PreparedStatement.RETURN_GENERATED_KEYS)) {
            statement.setString(1, client.getName());
            statement.setString(2, client.getSurname());
            statement.setInt(3, 0);
            statement.setString(4, client.getPhone());
            statement.setString(5, "");
            statement.execute();
            ResultSet rs = statement.getGeneratedKeys();
            rs.next();
            client.setId(rs.getLong(1));
        } catch (SQLException e) {
            System.out.println("Client doesn't saved!");
        }
        return client;
    }

    @Override
    public Client getClient(Long id) {
        Client result = null;
        try (Connection connection = new ConnectionManager().getConnection();
             PreparedStatement statement = connection.prepareStatement(SELECT_ONE_CLIENT)) {
            statement.setLong(1, id);
            ResultSet resultSet = statement.executeQuery();
            while (resultSet.next()) {
                result = new Client(resultSet.getLong(1),
                        resultSet.getString(2),
                        resultSet.getString(3),
                        resultSet.getInt(4),
                        resultSet.getString(5),
                        resultSet.getString(6));
            }
        } catch (SQLException e) {
            System.out.println("Client doesn't exist!");
        }
        return result;
    }

    @Override
    public List<Client> getAllClients() {
        List<Client> result = new ArrayList<>();
        try (Connection connection = new ConnectionManager().getConnection();
             Statement statement = connection.createStatement()) {
            try (ResultSet resultSet = statement.executeQuery(SELECT_ALL_CLIENTS)) {
                while (resultSet.next()) {
                    result.add(new Client(resultSet.getLong(1),
                            resultSet.getString(2),
                            resultSet.getString(3),
                            resultSet.getInt(4),
                            resultSet.getString(5),
                            resultSet.getString(6)));
                }
            } catch (SQLException e) {
                System.out.println("There are no clients!");
            }
        } catch (SQLException e) {
            System.out.println("Clients don't find!");
        }
        return result;
    }

    @Override
    public Client updateClient(Client client) {
        try (Connection connection = new ConnectionManager().getConnection();
             PreparedStatement statement = connection.prepareStatement(UPDATE_CLIENT)) {
            statement.setString(1, client.getName());
            statement.setString(2, client.getSurname());
            statement.setInt(3, client.getAge());
            statement.setString(4, client.getPhone());
            statement.setString(5, client.getEmail());
            statement.setLong(6, client.getId());
            statement.execute();
        } catch (SQLException e) {
            System.out.println("Client doesn't updated!");
        }
        return client;
    }

    @Override
    public Client deleteClient(Long id) {
        Client result = getClient(id);
        if(result == null)
            return result;
        try (Connection connection = new ConnectionManager().getConnection();
             PreparedStatement statement = connection.prepareStatement(DELETE_CLIENT)) {
            statement.setLong(1, id);
            int i = statement.executeUpdate();
            if (i < 1)
                return result;
        } catch (SQLException e) {
            System.out.println("Client doesn't deleted!");
        }
        return result;
    }
}

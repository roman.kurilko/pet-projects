package com.kurilko.dao.impl;

import com.kurilko.dao.ClientDao;
import com.kurilko.domain.Client;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ClientDaoImpl implements ClientDao {
    private static volatile ClientDaoImpl clientDaoImpl;
    private Map<Long, Client> map = new HashMap();
    private static Long generator = 1L;

    private ClientDaoImpl() {
    }

    public static ClientDaoImpl getInstance() {
        ClientDaoImpl localInstance = clientDaoImpl;
        if (localInstance == null) {
            synchronized (ClientDaoImpl.class) {
                localInstance = clientDaoImpl;
                if (localInstance == null) {
                    clientDaoImpl = localInstance = new ClientDaoImpl();
                }
            }
        }
        return localInstance;
    }

    @Override
    public Client saveClient(Client client) {
        client.setId(generator++);
        map.put(client.getId(), client);
        return map.get(generator - 1);
    }

    @Override
    public Client getClient(Long id) {
        return map.get(id);
    }

    public List<Client> getAllClients() {
        return new ArrayList<>(map.values());
    }

    @Override
    public Client updateClient(Client client) {
        return map.put(client.getId(), client);
    }

    @Override
    public Client deleteClient(Long id) {
        return map.remove(id);
    }
}

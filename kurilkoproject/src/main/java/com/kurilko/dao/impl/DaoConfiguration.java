package com.kurilko.dao.impl;

import com.kurilko.dao.ClientDao;
import com.kurilko.dao.OrderDao;
import com.kurilko.dao.ProductDao;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class DaoConfiguration {

    @Bean("clientEMDao")
    ClientDao ClientEMDao() {
        return new ClientEMDao();
    }

    @Bean("productEMDao")
    ProductDao ProductEMDao() {
        return new ProductEMDao();
    }

    @Bean("orderEMDao")
    OrderDao OrderEMDao() {
        return new OrderEMDao();
    }
}

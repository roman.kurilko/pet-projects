package com.kurilko.dao.impl;

import com.kurilko.dao.ProductDao;
import com.kurilko.domain.Product;

import java.util.*;
import java.util.stream.Collectors;

public class ProductDaoImpl implements ProductDao {
    private static volatile ProductDaoImpl productDaoImpl;
    private Map<Long, Product> map = new HashMap();
    private static Long generator = 1L;

    private ProductDaoImpl() {
    }

    public static ProductDaoImpl getInstance() {
        ProductDaoImpl localInstance = productDaoImpl;
        if (localInstance == null) {
            synchronized (OrderDaoImpl.class) {
                localInstance = productDaoImpl;
                if (localInstance == null) {
                    productDaoImpl = localInstance = new ProductDaoImpl();
                }
            }
        }
        return localInstance;
    }

    @Override
    public Product saveProduct(Product product) {
        product.setId(generator++);
        map.put(product.getId(), product);
        return map.get(generator - 1);
    }

    @Override
    public Product getProduct(Long id) {
        return map.get(id);
    }

    @Override
    public List<Product> getAllProducts() {
        return map.values().stream().collect(Collectors.toList());
    }

    @Override
    public List<Product> getProducts(List<Long> ids) {
        ArrayList<Product> products = new ArrayList<>();
        for (Long id : ids) {
            products.add(map.get(id));
        }
        return products;
    }

    @Override
    public Product updateProduct(Product product) {
        return map.put(product.getId(), product);
    }

    @Override
    public Product deleteProduct(Long id) {
        return map.remove(id);
    }
}

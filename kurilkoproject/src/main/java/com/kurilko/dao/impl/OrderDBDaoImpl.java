package com.kurilko.dao.impl;

import com.kurilko.dao.ClientDao;
import com.kurilko.dao.OrderDao;
import com.kurilko.dao.ProductDao;
import com.kurilko.domain.Order;
import com.kurilko.domain.Product;

import java.sql.*;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

public class OrderDBDaoImpl implements OrderDao {
    private final static String CREATE_TABLE_ORDER;
    private final static String INSERT_NEW_ORDER;
    private final static String INSERT_NEW_PRODUCTS_IN_ORDER;
    private final static String SELECT_ALL_ORDERS;
    private final static String SELECT_ORDERS_BY_CLIENT;
    private final static String SELECT_ONE_ORDER;
    private final static String SELECT_ORDER_PRODUCTS;
    private final static String UPDATE_ORDER;
    private final static String DELETE_ORDER;
    private final static String DELETE_PRODUCTS_FROM_ORDER;
    private final static DateFormat dateFormatMDY = new SimpleDateFormat("yyyy-MM-dd");

    private static final ProductDao productDao = new ProductDBDaoImpl();
    private static final ClientDao clientDao = new ClientDBDaoImpl();

    static {
        CREATE_TABLE_ORDER = "create table if not exists ORDERS \n" +
                "(ID BIGINT PRIMARY KEY AUTO_INCREMENT,\n" +
                "CLIENT BIGINT ,\n" +
                "ORDERDATE DATE);\n" +
                "\n" +
                "create table if not exists PRODUCTS_IN_ORDERS \n" +
                "(ID BIGINT PRIMARY KEY AUTO_INCREMENT,\n" +
                "ORDERID BIGINT ,\n" +
                "PRODUCTID BIGINT,\n" +
                "FOREIGN KEY(ORDERID) REFERENCES ORDERS (id),\n" +
                "FOREIGN KEY(PRODUCTID) REFERENCES PRODUCT(id));";
        INSERT_NEW_ORDER = "insert into ORDERS (CLIENT ,ORDERDATE  ) \n" +
                "values (?,?);";
        INSERT_NEW_PRODUCTS_IN_ORDER = "insert into PRODUCTS_IN_ORDERS (ORDERID , PRODUCTID) \n" +
                "values (?,?);";
        SELECT_ALL_ORDERS = "select \n" +
                "ID, " +
                "CLIENT, " +
                "ORDERDATE " +
                "FROM ORDERS";
        SELECT_ORDERS_BY_CLIENT = "select \n" +
                "ID, " +
                "CLIENT, " +
                "ORDERDATE " +
                "FROM ORDERS " +
                "WHERE CLIENT=?";
        SELECT_ONE_ORDER = "select \n" +
                "ID, " +
                "CLIENT, " +
                "ORDERDATE " +
                "FROM ORDERS " +
                "WHERE ID=?";
        SELECT_ORDER_PRODUCTS = "select \n" +
                "ID, " +
                "ORDERID, " +
                "PRODUCTID " +
                "FROM PRODUCTS_IN_ORDERS " +
                "WHERE ORDERID=?";
        UPDATE_ORDER = "UPDATE ORDERS\n" +
                "SET CLIENT = ?,\n" +
                "ORDERDATE = ?\n" +
                "WHERE ID=?";
        DELETE_ORDER = "DELETE FROM ORDERS WHERE ID=?";
        DELETE_PRODUCTS_FROM_ORDER = "DELETE FROM PRODUCTS_IN_ORDERS WHERE ORDERID=?";
    }

    public OrderDBDaoImpl() {
        try (Connection connection = new ConnectionManager().getConnection();
             Statement statement = connection.createStatement()) {
            statement.execute(CREATE_TABLE_ORDER);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public Order saveOrder(Order order) {
        try (Connection connection = new ConnectionManager().getConnection();
             PreparedStatement statement = connection.prepareStatement(INSERT_NEW_ORDER, PreparedStatement.RETURN_GENERATED_KEYS)) {
            statement.setLong(1, order.getClient().getId());
            DateFormat dateFormatMDY = new SimpleDateFormat("yyyy-MM-dd");
            statement.setString(2, dateFormatMDY.format(order.getDate()));
            statement.execute();
            ResultSet rs = statement.getGeneratedKeys();
            rs.next();
            order.setId(rs.getLong(1));
        } catch (SQLException e) {
            System.out.println("Order doesn't saved!");
        }
        for (Product product : order.getProducts()) {
            try (Connection connection = new ConnectionManager().getConnection();
                 PreparedStatement statement = connection.prepareStatement(INSERT_NEW_PRODUCTS_IN_ORDER, PreparedStatement.RETURN_GENERATED_KEYS)) {
                statement.setLong(1, order.getId());
                statement.setLong(2, product.getId());
                statement.execute();
            } catch (SQLException e) {
                System.out.println("Product doesn't saved!");
            }
        }
        return order;
    }

    @Override
    public Order getOrder(Long id) {
        Order result = null;
        try (Connection connection = new ConnectionManager().getConnection();
             PreparedStatement statement = connection.prepareStatement(SELECT_ONE_ORDER)) {
            statement.setLong(1, id);
            ResultSet resultSet = statement.executeQuery();
            while (resultSet.next()) {
                result = new Order(resultSet.getLong(1),
                        clientDao.getClient(resultSet.getLong(2)),
                        new ArrayList<Product>(),
                        resultSet.getDate(3));
            }
        } catch (SQLException e) {
            e.printStackTrace();
            System.out.println("Order doesn't exist!");
        }
        ArrayList<Product> orderProducts = getProductsFromOrder(id);
        result.setProducts(orderProducts);
        return result;
    }

    @Override
    public List<Order> getAllOrders() {
        ArrayList<Order> result = new ArrayList<>();
        try (Connection connection = new ConnectionManager().getConnection();
             PreparedStatement statement = connection.prepareStatement(SELECT_ALL_ORDERS)) {
            ResultSet resultSet = statement.executeQuery();
            while (resultSet.next()) {
                result.add(new Order(resultSet.getLong(1),
                        clientDao.getClient(resultSet.getLong(2)),
                        new ArrayList<Product>(),
                        resultSet.getDate(3)));
            }
        } catch (SQLException e) {
            e.printStackTrace();
            System.out.println("Order doesn't exist!");
        }
        for (Order order : result) {
            ArrayList<Product> orderProducts = getProductsFromOrder(order.getId());
            order.setProducts(orderProducts);
        }
        return result;
    }

    @Override
    public List<Order> getOrders(Long clientId) {
        ArrayList<Order> result = new ArrayList<>();
        try (Connection connection = new ConnectionManager().getConnection();
             PreparedStatement statement = connection.prepareStatement(SELECT_ORDERS_BY_CLIENT)) {
            statement.setLong(1, clientId);
            ResultSet resultSet = statement.executeQuery();
            while (resultSet.next()) {
                result.add(new Order(resultSet.getLong(1),
                        clientDao.getClient(resultSet.getLong(2)),
                        new ArrayList<Product>(),
                        resultSet.getDate(3)));
            }
        } catch (SQLException e) {
            e.printStackTrace();
            System.out.println("Order doesn't exist!");
        }
        for (Order order : result) {
            ArrayList<Product> orderProducts = getProductsFromOrder(order.getId());
            order.setProducts(orderProducts);
        }
        return result;
    }

    private ArrayList<Product> getProductsFromOrder(Long id) {
        ArrayList<Product> orderProducts = new ArrayList<>();
        try (Connection connection = new ConnectionManager().getConnection();
             PreparedStatement statement = connection.prepareStatement(SELECT_ORDER_PRODUCTS)) {
            statement.setLong(1, id);
            ResultSet resultSet = statement.executeQuery();
            while (resultSet.next()) {
                orderProducts.add(productDao.getProduct(resultSet.getLong(3)));
            }
        } catch (SQLException e) {
            e.printStackTrace();
            System.out.println("Products don't exist!");
        }
        return orderProducts;
    }

    @Override
    public Order updateOrder(Order order) {
        try (Connection connection = new ConnectionManager().getConnection();
             PreparedStatement statement = connection.prepareStatement(UPDATE_ORDER, PreparedStatement.RETURN_GENERATED_KEYS)) {
            statement.setLong(1, order.getClient().getId());

            statement.setString(2, dateFormatMDY.format(order.getDate()));
            int i = statement.executeUpdate();
            if (i < 1)
                return null;
        } catch (SQLException e) {
            System.out.println("Order doesn't saved!");
        }
        deleteProductsFromOrder(order.getId());
        for (Product product : order.getProducts()) {
            try (Connection connection = new ConnectionManager().getConnection();
                 PreparedStatement statement = connection.prepareStatement(INSERT_NEW_PRODUCTS_IN_ORDER, PreparedStatement.RETURN_GENERATED_KEYS)) {
                statement.setLong(1, order.getId());
                statement.setLong(2, product.getId());
                statement.execute();
            } catch (SQLException e) {
                System.out.println("Product doesn't saved!");
            }
        }
        return order;
    }

    @Override
    public Order deleteOrder(Long id) {
        Order result = getOrder(id);
        if (result == null)
            return result;
        deleteProductsFromOrder(id);
        try (Connection connection = new ConnectionManager().getConnection();
             PreparedStatement statement = connection.prepareStatement(DELETE_ORDER)) {
            statement.setLong(1, id);
            int i = statement.executeUpdate();
            if (i < 1)
                return result;
        } catch (SQLException e) {
            System.out.println("Order doesn't deleted!");
        }
        return result;
    }

    private void deleteProductsFromOrder(Long id) {
        try (Connection connection = new ConnectionManager().getConnection();
             PreparedStatement statement = connection.prepareStatement(DELETE_PRODUCTS_FROM_ORDER)) {
            statement.setLong(1, id);
            statement.executeUpdate();
        } catch (SQLException e) {
            System.out.println("Products don't deleted!");
        }
    }
}

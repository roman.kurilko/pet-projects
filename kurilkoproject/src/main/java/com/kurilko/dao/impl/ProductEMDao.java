package com.kurilko.dao.impl;

import com.kurilko.dao.ProductDao;
import com.kurilko.domain.Product;
import org.springframework.beans.factory.annotation.Autowired;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.TypedQuery;
import java.util.List;

public class ProductEMDao implements ProductDao {
    private EntityManager entityManager;

    @Autowired
    public ProductEMDao() {
        EntityManagerFactory factory = Persistence.createEntityManagerFactory("persistence-unit");
        this.entityManager = factory.createEntityManager();
    }

    @Override
    public Product saveProduct(Product product) {
        entityManager.getTransaction().begin();
        entityManager.persist(product);
        entityManager.getTransaction().commit();
        return product;
    }

    @Override
    public Product getProduct(Long id) {
        Product product = entityManager.find(Product.class, id);
        return product;
    }

    @Override
    public List<Product> getAllProducts() {
        List<Product> products = entityManager.createQuery("from Product", Product.class).getResultList();
        return products;
    }

    @Override
    public List<Product> getProducts(List<Long> ids) {
        String queryString = "from Product where id in :ids";
        TypedQuery<Product> query = entityManager.createQuery(queryString, Product.class);
        query.setParameter("ids", ids);
        List<Product> products = query.getResultList();
        return products;
    }

    @Override
    public Product updateProduct(Product product) {
        entityManager.getTransaction().begin();
        entityManager.merge(product);
        entityManager.getTransaction().commit();
        return product;
    }

    @Override
    public Product deleteProduct(Long id) {
        entityManager.getTransaction().begin();
        Product deletedProduct = entityManager.find(Product.class, id);
        entityManager.remove(deletedProduct);
        entityManager.getTransaction().commit();
        return deletedProduct;
    }
}

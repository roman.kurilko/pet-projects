package com.kurilko.dao.impl;

import com.kurilko.dao.OrderDao;
import com.kurilko.domain.Order;

import java.util.*;
import java.util.stream.Collectors;

public class OrderDaoImpl implements OrderDao {
    private static volatile OrderDaoImpl orderDaoImpl;
    private Map<Long, Order> map = new HashMap();
    private static Long generator = 1L;

    private OrderDaoImpl() {
    }

    public static OrderDaoImpl getInstance() {
        OrderDaoImpl localInstance = orderDaoImpl;
        if (localInstance == null) {
            synchronized (OrderDaoImpl.class) {
                localInstance = orderDaoImpl;
                if (localInstance == null) {
                    orderDaoImpl = localInstance = new OrderDaoImpl();
                }
            }
        }
        return localInstance;
    }

    @Override
    public Order saveOrder(Order order) {
        order.setId(generator++);
        map.put(order.getId(), order);
        return map.get(generator - 1);
    }

    @Override
    public Order getOrder(Long id) {
        return map.get(id);
    }

    @Override
    public List<Order> getAllOrders() {
        return map.values().stream().collect(Collectors.toList());
    }


    @Override
    public List<Order> getOrders(Long clientId) {
        ArrayList<Order> result = new ArrayList<>();
        for (Order order : map.values()) {
            if (order.getClient().getId().equals(clientId)) {
                result.add(order);
            }
        }
        return result;
    }

    @Override
    public Order updateOrder(Order order) {
        return map.put(order.getId(), order);
    }

    @Override
    public Order deleteOrder(Long id) {
        return map.remove(id);
    }
}

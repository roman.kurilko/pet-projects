package com.kurilko.dao;

import com.kurilko.domain.Product;

import java.util.List;

public interface ProductDao {
    /**
     * Method saves new product in the storage
     *
     * @param product takes instance of Product to save it
     * @return true if saving was successful and false if there wasn't
     */
    Product saveProduct(Product product);

    /**
     * Method gets instance of Product by his id from the storage
     *
     * @param id is an id of product that we are getting
     * @return instance of Product
     */
    Product getProduct(Long id);

    /**
     * Method gets list of products to show
     *
     * @return list of Products
     */
    List<Product> getAllProducts();

    /**
     * Method gets list of products to show
     *
     * @param ids is a collection of ids
     * @return list of Products
     */
    List<Product> getProducts(List<Long> ids);

    /**
     * Method updates existing instance of the Product by new instance, that is received from the parameter
     *
     * @param product new instance of the Product that will replace old one
     * @return replaced new instance of the Product
     */
    Product updateProduct(Product product);

    /**
     * Method deletes existing instance of the Product by id, that is received from the parameter
     *
     * @param id is an id of deleting instance from the storage
     * @return instance of deleted Product, that we can show after deleting operation
     */
    Product deleteProduct(Long id);
}

package com.kurilko.dao;

import com.kurilko.domain.Client;

import java.util.List;

public interface ClientDao {
    /**
     * Method saves new client in the storage
     *
     * @param client takes instance of Client to save it
     * @return true if saving was successful and false if there wasn't
     */
    Client saveClient(Client client);

    /**
     * Method gets instance of Client by his id from the storage
     *
     * @param id is an id of client that we are getting
     * @return instance of Client
     */
    Client getClient(Long id);

    /**
     * Method gets instances of all Clients
     *
     * @return List of Clients
     */
    List<Client> getAllClients();

    /**
     * Method updates existing instance of the Client by new instance, that is received from the parameter
     *
     * @param client new instance of the Client that will replace old one
     * @return replaced new instance of the Client
     */
    Client updateClient(Client client);

    /**
     * Method deletes existing instance of the Client by id, that is received from the parameter
     *
     * @param id is an id of deleting instance from the storage
     * @return instance of deleted Client, that we can show after deleting operation
     */
    Client deleteClient(Long id);
}

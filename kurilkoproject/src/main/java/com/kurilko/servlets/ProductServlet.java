package com.kurilko.servlets;

import com.kurilko.domain.Product;
import com.kurilko.services.ProductService;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.math.BigDecimal;

public class ProductServlet extends HttpServlet {
    private ProductService productService;

    public ProductServlet(ProductService productService) {
        this.productService = productService;
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        resp.setContentType("text/html");
        PrintWriter writer = resp.getWriter();
        for (Product product : productService.getProducts(0, 20)) {
            writer.println("<div>" + product + "</div>");
        }
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        PrintWriter writer = resp.getWriter();
        if (req.getParameter("name") != null &&
                req.getParameter("price") != null) {
            String name = req.getParameter("name");
            BigDecimal price = new BigDecimal(req.getParameter("price"));
            Product product = productService.createProduct(new Product(null, name, price));
            writer.println("<div>Added product:</div>");
            writer.println("<div>" + product.toString() + "</div>");
        } else {
            writer.println("<div>Wrong data! You should provide full information!</div>");
        }
    }

    @Override
    protected void doPut(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        PrintWriter writer = resp.getWriter();
        if (req.getParameter("id") != null &&
                req.getParameter("name") != null &&
                req.getParameter("price") != null) {
            Long id = new Long(0);
            try {
                id = Long.parseLong(req.getParameter("id"));
            } catch (NumberFormatException e) {
                e.printStackTrace();
                writer.println("<div>Wrong id!</div>");
                return;
            }
            String name = req.getParameter("name");
            BigDecimal price = new BigDecimal(req.getParameter("price"));
            Product product = productService.updateProduct(new Product(id, name, price));
            writer.println("<div>Updated product:</div>");
            writer.println("<div>" + product.toString() + "</div>");
        } else {
            writer.println("<div>Wrong data! You should provide full information!</div>");
        }
    }

    @Override
    protected void doDelete(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        PrintWriter writer = resp.getWriter();
        Product product = productService.deleteProduct(ReadData.getId(req));
        writer.println("<div>Deleted product:</div>");
        writer.println("<div>" + product.toString() + "</div>");
    }
}

package com.kurilko.servlets;

import com.kurilko.domain.Client;
import com.kurilko.services.ClientService;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

public class ClientServlet extends HttpServlet {
    private ClientService clientService;

    public ClientServlet(ClientService clientService) {
        this.clientService = clientService;
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        resp.setContentType("text/html");
        PrintWriter writer = resp.getWriter();
        for (Client client : clientService.getAllClients()) {
            writer.println("<div>" + client + "</div>");
        }
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        PrintWriter writer = resp.getWriter();
        if (req.getParameter("name") != null &&
                req.getParameter("surname") != null &&
                req.getParameter("phone") != null &&
                req.getParameter("age") != null &&
                req.getParameter("email") != null) {
            String name = req.getParameter("name");
            String surname = req.getParameter("surname");
            String age = req.getParameter("age");
            String phone = req.getParameter("phone");
            String email = req.getParameter("email");
            Client client = clientService.createClient(new Client(null, name, surname, Integer.parseInt(age), phone, email));
            writer.println("<div>Added client:</div>");
            writer.println("<div>" + client.toString() + "</div>");
        } else {
            writer.println("<div>Wrong data! You should provide full information!</div>");
        }
    }

    @Override
    protected void doPut(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        PrintWriter writer = resp.getWriter();
        if (req.getParameter("id") != null &&
                req.getParameter("name") != null &&
                req.getParameter("surname") != null &&
                req.getParameter("phone") != null &&
                req.getParameter("age") != null &&
                req.getParameter("email") != null) {
            Long id = new Long(0);
            try {
                id = Long.parseLong(req.getParameter("id"));
            } catch (NumberFormatException e) {
                e.printStackTrace();
                writer.println("<div>Wrong id!</div>");
                return;
            }
            String name = req.getParameter("name");
            String surname = req.getParameter("surname");
            String age = req.getParameter("age");
            String phone = req.getParameter("phone");
            String email = req.getParameter("email");
            Client client = clientService.updateClient(new Client(id, name, surname, Integer.parseInt(age), phone, email));
            writer.println("<div>Updated client:</div>");
            writer.println("<div>" + client.toString() + "</div>");
        } else {
            writer.println("<div>Wrong data! You should provide full information!</div>");
        }
    }

    @Override
    protected void doDelete(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        PrintWriter writer = resp.getWriter();
        Client client = clientService.deleteClient(ReadData.getId(req));
        writer.println("<div>Deleted client:</div>");
        writer.println("<div>" + client.toString() + "</div>");
    }
}


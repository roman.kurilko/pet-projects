package com.kurilko.servlets;

import javax.servlet.http.HttpServletRequest;

public class ReadData {
    public static Long getId(HttpServletRequest req) {
        Long id = new Long(0L);
        if(req.getParameter("id") != null) {
            try {
                id = Long.parseLong(req.getParameter("id"));
            } catch (NumberFormatException e) {
                e.printStackTrace();
                return 0L;
            }
        }
        return id;
    }
}

package com.kurilko.servlets;

import com.kurilko.domain.Client;
import com.kurilko.domain.Order;
import com.kurilko.domain.Product;
import com.kurilko.services.ClientService;
import com.kurilko.services.OrderService;
import com.kurilko.services.ProductService;
import com.kurilko.services.impl.Conversion;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

public class OrderServlet extends HttpServlet {
    private OrderService orderService;
    private ClientService clientService;
    private ProductService productService;
    private final static DateFormat dateFormatMDY = new SimpleDateFormat("yyyy-MM-dd");

    public OrderServlet(OrderService orderService, ClientService clientService, ProductService productService) {
        this.orderService = orderService;
        this.clientService = clientService;
        this.productService = productService;
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        resp.setContentType("text/html");
        PrintWriter writer = resp.getWriter();
        for (Order order : orderService.getOrders(0, 20)) {
            writer.println("<div>" + order + "</div>");
        }
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        PrintWriter writer = resp.getWriter();
        if (req.getParameter("client") != null &&
                req.getParameter("products") != null &&
                req.getParameter("date") != null) {
            Client client = clientService.readClient(Long.parseLong(req.getParameter("client")));
            List<Product> products = productService.getProducts(Conversion.stringArrayToLong(req.getParameter("products").split(",")));
            Date date = new Date();
            try {
                date = dateFormatMDY.parse(req.getParameter("date"));
            } catch (ParseException e) {
                e.printStackTrace();
                return;
            }
            Order order = orderService.createOrder(new Order(null, client, products, date));
            writer.println("<div>Added order:</div>");
            writer.println("<div>" + order.toString() + "</div>");
        } else {
            writer.println("<div>Wrong data! You should provide full information!</div>");
        }
    }

    @Override
    protected void doPut(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        PrintWriter writer = resp.getWriter();
        if (req.getParameter("id") != null &&
                req.getParameter("client") != null &&
                req.getParameter("products") != null &&
                req.getParameter("date") != null) {
            Long id = ReadData.getId(req);
            Client client = clientService.readClient(Long.parseLong(req.getParameter("client")));
            List<Product> products = productService.getProducts(Conversion.stringArrayToLong(req.getParameter("products").split(",")));
            Date date = new Date();
            try {
                date = dateFormatMDY.parse(req.getParameter("date"));
            } catch (ParseException e) {
                e.printStackTrace();
                return;
            }
            Order order = orderService.updateOrder(new Order(id, client, products, date));
            writer.println("<div>Updated order:</div>");
            writer.println("<div>" + order.toString() + "</div>");
        } else {
            writer.println("<div>Wrong data! You should provide full information!</div>");
        }
    }

    @Override
    protected void doDelete(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        PrintWriter writer = resp.getWriter();
        Order order = orderService.deleteOrder(ReadData.getId(req));
        writer.println("<div>Deleted order:</div>");
        writer.println("<div>" + order.toString() + "</div>");
    }
}

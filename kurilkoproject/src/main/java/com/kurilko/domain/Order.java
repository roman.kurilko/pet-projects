package com.kurilko.domain;


import com.fasterxml.jackson.annotation.JsonManagedReference;

import javax.persistence.*;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Objects;

@Entity
@Table(name = "ORDERS")
public class Order {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID")
    private Long id;
    @ManyToOne
    @JoinColumn(name = "client")
    @JsonManagedReference
    private Client client;
    @OneToMany(cascade = CascadeType.MERGE, fetch = FetchType.EAGER)
    @JoinTable(
            name = "PRODUCTS_IN_ORDERS",
            joinColumns = @JoinColumn(
                    name = "ORDERID",
                    referencedColumnName = "ID"
            ),
            inverseJoinColumns = @JoinColumn(
                    name = "PRODUCTID",
                    referencedColumnName = "ID"
            )
    )
    @JsonManagedReference
    private List<Product> products;
    @Temporal(TemporalType.DATE)
    @Column(name = "ORDERDATE")
    private Date date;
    private final static DateFormat dateFormatMDY = new SimpleDateFormat("yyyy-MM-dd");

    public Order() {
    }

    public Order(Client client, List<Product> products, Date date) {
        this.client = client;
        this.products = products;
        this.date = date;
    }

    public Order(Long id, Client client, List<Product> products, Date date) {
        this.id = id;
        this.client = client;
        this.products = products;
        this.date = date;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Override
    public String toString() {
        return "Order{" +
                "id=" + id +
                ", client=" + client +
                ", products=" + products +
                ", date=" + date +
                '}';
    }

    public Client getClient() {
        return client;
    }

    public void setClient(Client client) {
        this.client = client;
    }

    public List<Product> getProducts() {
        return products;
    }

    public void setProducts(List<Product> products) {
        this.products = products;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Order)) return false;
        Order order = (Order) o;
        return getId().equals(order.getId()) &&
                getClient().equals(order.getClient()) &&
                getProducts().equals(order.getProducts()) &&
                getDate().equals(order.getDate());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getId(), getClient(), getProducts(), getDate());
    }

    public static DateFormat getDateFormatMDY() {
        return dateFormatMDY;
    }
}

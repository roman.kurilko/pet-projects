package com.kurilko.view;

import com.kurilko.domain.Client;
import com.kurilko.services.ClientService;
import com.kurilko.services.OrderService;
import com.kurilko.services.ProductService;
import com.kurilko.services.impl.ReadData;
import com.kurilko.validators.ValidationService;

import java.io.BufferedReader;
import java.util.List;

public class AdminMenu {
    private final BufferedReader br;
    private final ClientService clientService;
    private final OrderService orderService;
    private final ProductService productService;
    private final ValidationService validationService;

    public AdminMenu(BufferedReader br, ClientService clientService, OrderService orderService, ProductService productService, ValidationService validationService) {
        this.br = br;
        this.clientService = clientService;
        this.orderService = orderService;
        this.productService = productService;
        this.validationService = validationService;
    }

    void show() {
        boolean isRunningAdminMenu = true;
        while (isRunningAdminMenu) {
            showMenu();
            switch (ReadData.readString(br)) {
                case "1": {
                    System.out.println("All clients:");
                    List<Client> allClients = clientService.getAllClients();
                    if (allClients.size() == 0) {
                        System.out.println("There are no clients yet!\r\n");
                    } else {
                        allClients.forEach(System.out::println);
                        System.out.println("");
                    }
                    break;
                }
                case "2": {
                    ClientManagement clientManagement = new ClientManagement(br, clientService, validationService);
                    Long id = clientManagement.createClient();
                    break;
                }
                case "3": {
                    System.out.println("Modify client");
                    ClientManagement clientManagement = new ClientManagement(br, clientService, validationService);
                    clientManagement.updateClient();
                    break;
                }
                case "4": {
                    removeClient();
                    break;
                }
                case "5": {
                    System.out.println("Operations with content");
                    ContentManagement.contentMenu();
                    break;
                }
                case "6": {
                    OrdersManagement ordersManagement = new OrdersManagement(br, clientService, orderService, productService);
                    ordersManagement.ordersMenu();
                    break;
                }

                case "0": {
                    System.out.println("Return to the previous menu");
                    isRunningAdminMenu = false;
                    break;
                }
                default: {
                    System.out.println("Wrong parameter");
                    break;
                }
            }
        }
    }

    private void removeClient() {
        System.out.println("Remove client");
        System.out.println("Input id of client: ");
        Long id = ReadData.readLong(br);
        clientService.deleteClient(id);
    }

    private void showMenu() {
        System.out.println("1. All clients");
        System.out.println("2. Add client");
        System.out.println("3. Modify client");
        System.out.println("4. Remove client");
        System.out.println("5. Content management");
        System.out.println("6. Orders");
        System.out.println("0. Previous menu");
    }
}

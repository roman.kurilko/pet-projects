package com.kurilko.view;

import com.kurilko.dao.impl.ProductDBDaoImpl;
import com.kurilko.domain.Product;
import com.kurilko.services.ProductService;
import com.kurilko.services.impl.ProductServiceImpl;
import com.kurilko.services.impl.ReadData;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.math.BigDecimal;
import java.util.List;

class ContentManagement {
    private static final BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
    private static final ProductService productService = new ProductServiceImpl(new ProductDBDaoImpl());

    static void contentMenu() {
        boolean isRunningContentMenu = true;
        while (isRunningContentMenu) {
            System.out.println("1. List products");
            System.out.println("2. Add product");
            System.out.println("3. Modify product");
            System.out.println("4. Remove product");
            System.out.println("0. Previous menu");
            switch (ReadData.readString(br)) {
                case "1": {
                    showProducts();
                    break;
                }
                case "2": {
                    System.out.println("Input name of product:");
                    String name = ReadData.readString(br);
                    System.out.println("Input price:");
                    BigDecimal price = ReadData.readBigDecimal(br);
                    Product product = productService.createProduct(new Product(name, price));
                    System.out.println("Created:" + product.toString());
                    break;
                }
                case "3": {
                    System.out.println("Input id:");
                    Long id = ReadData.readLong(br);
                    System.out.println("Input name:");
                    String name = ReadData.readString(br);
                    System.out.println("Input price:");
                    BigDecimal price = ReadData.readBigDecimal(br);
                    Product newProduct = new Product(id, name, price);
                    Product product = productService.updateProduct(newProduct);
                    if(product != null) {
                        System.out.println("Updated:" + newProduct.toString());
                    }
                    break;
                }
                case "4": {
                    System.out.println("Input id:");
                    Long id = ReadData.readLong(br);
                    Product product = productService.deleteProduct(id);
                    System.out.println("Deleted:" + product.toString());
                    break;
                }
                case "0": {
                    System.out.println("Return to the previous menu");
                    isRunningContentMenu = false;
                    break;
                }
                default: {
                    System.out.println("Please, enter proper number of menu!");
                    break;
                }
            }
        }
    }

    static void showProducts() {
        List<Product> products = productService.getProducts(0, 20);
        products.forEach(product-> System.out.println(product));
    }
}

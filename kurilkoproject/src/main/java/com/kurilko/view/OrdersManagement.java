package com.kurilko.view;

import com.kurilko.domain.Order;
import com.kurilko.domain.Product;
import com.kurilko.services.ClientService;
import com.kurilko.services.OrderService;
import com.kurilko.services.ProductService;
import com.kurilko.services.impl.Conversion;
import com.kurilko.services.impl.ReadData;

import java.io.BufferedReader;
import java.util.Date;
import java.util.List;

class OrdersManagement {
    private final BufferedReader br;
    private final ClientService clientService;
    private final OrderService orderService;
    private final ProductService productService;

    OrdersManagement(BufferedReader br, ClientService clientService, OrderService orderService, ProductService productService) {
        this.br = br;
        this.clientService = clientService;
        this.orderService = orderService;
        this.productService = productService;
    }

    void ordersMenu() {
        boolean isRunningOrdersMenu = true;
        while (isRunningOrdersMenu) {
            System.out.println("1. List orders");
            System.out.println("2. Add order");
            System.out.println("3. Modify order");
            System.out.println("4. Remove order");
            System.out.println("0. Previous menu");
            switch (ReadData.readString(br)) {
                case "1": {
                    List<Order> orders = orderService.getOrders(0, 20);
                    orders.forEach(order -> System.out.println(order));
                    break;
                }
                case "2": {
                    Long id = createOrder(-1L);
                    break;
                }
                case "3": {
                    System.out.println("Input id of client:");
                    Long idClient = ReadData.readLong(br);
                    Long id = updateOrder(idClient);
                    break;
                }
                case "4": {
                    System.out.println("Input id:");
                    Long id = ReadData.readLong(br);
                    Order order = orderService.deleteOrder(id);
                    System.out.println("Deleted:" + order.toString());
                    break;
                }
                case "0": {
                    System.out.println("Return to the previous menu");
                    isRunningOrdersMenu = false;
                    break;
                }
                default: {
                    System.out.println("Please, enter proper number of menu!");
                    break;
                }
            }
        }
    }

    Long createOrder(Long idClient) {
        if (idClient == -1L) {
            System.out.println("Input id of client:");
            idClient = ReadData.readLong(br);
        }
        System.out.println("Input ids of products (\",\" is delimiter):");
        List<Long> productsIds = Conversion.stringArrayToLong(ReadData.readString(br).split(","));
        List<Product> products = productService.getProducts(productsIds);
        Order order = orderService.createOrder(new Order(clientService.readClient(idClient), products, new Date()));
        System.out.println("Created:" + order.toString());
        return order.getId();
    }

    Long updateOrder(Long idClient) {
        System.out.println("Input order id:");
        Long id = ReadData.readLong(br);
        System.out.println("Input ids of products (\",\" is delimiter):");
        List<Product> products = productService.getProducts(Conversion.stringArrayToLong(ReadData.readString(br).split(",")));
        Order newOrder = new Order(id, clientService.readClient(idClient), products, new Date());
        Order order = orderService.updateOrder(newOrder);
        if (order != null) {
            System.out.println("Updated:" + newOrder.toString());
        }
        return order.getId();
    }
}
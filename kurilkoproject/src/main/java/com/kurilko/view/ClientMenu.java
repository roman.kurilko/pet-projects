package com.kurilko.view;

import com.kurilko.domain.Order;
import com.kurilko.services.ClientService;
import com.kurilko.services.OrderService;
import com.kurilko.services.ProductService;
import com.kurilko.services.impl.ReadData;
import com.kurilko.validators.ValidationService;

import java.io.BufferedReader;
import java.util.List;


public class ClientMenu {
    private final BufferedReader br;
    private final ClientService clientService;
    private final OrderService orderService;
    private final ProductService productService;
    private final ValidationService validationService;

    public ClientMenu(BufferedReader br, ClientService clientService, OrderService orderService, ProductService productService, ValidationService validationService) {
        this.br = br;
        this.clientService = clientService;
        this.orderService = orderService;
        this.productService = productService;
        this.validationService = validationService;
    }

    void show() {
        boolean isRunningClientMenu = true;
        while (isRunningClientMenu) {
            System.out.println("Our shop has next products for you:");
            ContentManagement.showProducts();
            System.out.println("1. Buy products");
            System.out.println("2. Modify your order");
            System.out.println("3. List your orders");
            System.out.println("0. Previous menu");
            switch (ReadData.readString(br)) {
                case "1": {
                    ClientManagement clientManagement = new ClientManagement(br, clientService, validationService);
                    Long idClient = clientManagement.createClient();
                    OrdersManagement ordersManagement = new OrdersManagement(br, clientService, orderService, productService);
                    Long idOrder = ordersManagement.createOrder(idClient);
                    System.out.println("Your order " + idOrder + " was registered!");
                    break;
                }
                case "2": {
                    ClientManagement clientManagement = new ClientManagement(br, clientService, validationService);
                    Long idClient = clientManagement.logInClient();
                    if(idClient == null)
                        break;
                    OrdersManagement ordersManagement = new OrdersManagement(br, clientService, orderService, productService);
                    Long idOrder = ordersManagement.updateOrder(idClient);
                    System.out.println("Your order " + idOrder + " was modified!");
                    break;
                }
                case "3": {
                    ClientManagement clientManagement = new ClientManagement(br, clientService, validationService);
                    Long idClient = clientManagement.logInClient();
                    if(idClient == null)
                        break;
                    List<Order> orders = orderService.getOrdersById(idClient);
                    orders.forEach(System.out::println);
                    System.out.println("");
                    break;
                }
                case "0": {
                    System.out.println("Return to the previous menu");
                    isRunningClientMenu = false;
                    break;
                }
                default: {
                    System.out.println("Wrong parameter");
                    break;
                }
            }
        }
    }
}

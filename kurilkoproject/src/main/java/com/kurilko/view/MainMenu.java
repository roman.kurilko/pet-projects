package com.kurilko.view;

import com.kurilko.services.impl.ReadData;

import java.io.BufferedReader;

public class MainMenu {
    private final BufferedReader br;
    private final AdminMenu adminMenu;
    private final ClientMenu clientMenu;

    public MainMenu(BufferedReader br, AdminMenu adminMenu, ClientMenu clientMenu) {
        this.br = br;
        this.adminMenu = adminMenu;
        this.clientMenu = clientMenu;
    }

    public void showMenu() {
        boolean isRunning = true;
        while (isRunning) {
            System.out.println("1. Admin");
            System.out.println("2. Client");
            System.out.println("0. Exit");
            switch (ReadData.readString(br)) {
                case "1": {
                    adminMenu.show();
                    break;
                }
                case "2": {
                    clientMenu.show();
                    break;
                }
                case "0": {
                    System.out.println("Goodbye!");
                    isRunning = false;
                    break;
                }
                default: {
                    System.out.println("Please, enter proper number of menu!");
                    break;
                }
            }
        }
    }
}

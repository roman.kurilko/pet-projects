package com.kurilko.view;

import com.kurilko.domain.Client;
import com.kurilko.services.ClientService;
import com.kurilko.services.impl.ReadData;
import com.kurilko.validators.ValidationService;

import java.io.BufferedReader;

class ClientManagement {
    private final BufferedReader br;
    private final ClientService clientService;
    private final ValidationService validationService;

    ClientManagement(BufferedReader br, ClientService clientService, ValidationService validationService) {
        this.br = br;
        this.clientService = clientService;
        this.validationService = validationService;
    }

    Long createClient() {
        System.out.println("Input name:");
        String name = ReadData.readString(br);
        System.out.println("Input surname:");
        String surname = ReadData.readString(br);
        System.out.println("Input phone:");
        String phone = ReadData.readString(br);
        Client client = clientService.createClient(new Client(name, surname, phone));
        return client.getId();
    }

    void updateClient() {
        System.out.println("Input id of modifying client:");
        Long id = ReadData.readLong(br);
        System.out.println("Input name:");
        String name = ReadData.readString(br);
        System.out.println("Input surname:");
        String surname = ReadData.readString(br);
        System.out.println("Input phone:");
        String phone = ReadData.readString(br);
        System.out.println("Input age:");
        int age = ReadData.readAge(br, validationService);
        System.out.println("Input email:");
        String email = ReadData.readString(br);
        Client client = clientService.updateClient(new Client(id, name, surname, age, phone, email));
    }

    Long logInClient() {
        System.out.println("Input id of client:");
        Long id = ReadData.readLong(br);
        return clientService.readClient(id).getId();
    }

}

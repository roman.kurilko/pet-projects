package com.kurilko;

import com.kurilko.dao.ClientDao;
import com.kurilko.dao.OrderDao;
import com.kurilko.dao.ProductDao;
import com.kurilko.dao.impl.*;
import com.kurilko.services.ClientService;
import com.kurilko.services.OrderService;
import com.kurilko.services.ProductService;
import com.kurilko.services.impl.ClientServiceImpl;
import com.kurilko.services.impl.OrderServiceImpl;
import com.kurilko.services.impl.ProductServiceImpl;
import com.kurilko.validators.ValidationService;
import com.kurilko.validators.ValidationServiceImpl;
import com.kurilko.view.AdminMenu;
import com.kurilko.view.ClientMenu;
import com.kurilko.view.MainMenu;
import org.h2.tools.Server;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.sql.SQLException;

public class App {
    public static void main(String[] args) {
        Server server = null;
        try {
            server = Server.createTcpServer("-tcpAllowOthers").start();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        ClientDao clientDao = new ClientEMDao();
        ValidationService validationService = new ValidationServiceImpl(clientDao);
        ClientService clientService = new ClientServiceImpl(clientDao, validationService);
        OrderDao orderDao = new OrderEMDao();
        OrderService orderService = new OrderServiceImpl(orderDao);
        ProductDao productDao = new ProductEMDao();
        ProductService productService = new ProductServiceImpl(productDao);
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        AdminMenu adminMenu = new AdminMenu(br, clientService, orderService, productService, validationService);
        ClientMenu clientMenu = new ClientMenu(br, clientService, orderService, productService, validationService);
        MainMenu menu = new MainMenu(br, adminMenu, clientMenu);
        menu.showMenu();
        server.stop();
    }
}

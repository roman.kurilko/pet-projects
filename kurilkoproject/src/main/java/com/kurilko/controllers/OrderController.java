package com.kurilko.controllers;

import com.kurilko.domain.Order;
import com.kurilko.services.OrderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
@RequestMapping(value = "/mvc-orders")
public class OrderController {
    @Autowired
    private OrderService orderService;

    @RequestMapping(method = RequestMethod.GET)
    public String showOrders(ModelMap modelMap) {
        modelMap.put("message", orderService.getOrders(0, 50));
        return "orders";
    }

    @RequestMapping(method = RequestMethod.POST)
    public Order addOrder(@RequestParam Long client,
                          @RequestParam String products,
                          @RequestParam String date) {
        return orderService.createOrder(client, products, date);
    }

    @RequestMapping(method = RequestMethod.PUT)
    public Order modifyOrder(@RequestParam Long id,
                             @RequestParam Long client,
                             @RequestParam String products,
                             @RequestParam String date) {
        return orderService.updateOrder(id, client, products, date);
    }

    @RequestMapping(method = RequestMethod.DELETE)
    public Order deleteOrder(@RequestParam Long id) {
        return orderService.deleteOrder(id);
    }
}

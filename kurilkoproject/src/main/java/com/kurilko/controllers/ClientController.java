package com.kurilko.controllers;

import com.kurilko.domain.Client;
import com.kurilko.services.ClientService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;


@RestController
@RequestMapping(value = "/mvc-clients", produces = MediaType.APPLICATION_JSON_VALUE)
public class ClientController {
    @Autowired
    private ClientService clientService;

    @RequestMapping(method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<List<Client>> showClients() {
        return new ResponseEntity<List<Client>> (clientService.getAllClients(), HttpStatus.OK);
    }

    @RequestMapping(value = "save", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public ResponseEntity<Client> addClient(@RequestBody Client client) {
        return new ResponseEntity<Client>(clientService.createClient(client), HttpStatus.OK);
    }

    @RequestMapping(method = RequestMethod.PUT)
    public Client modifyClient(@RequestParam Long id,
                               @RequestParam String name,
                               @RequestParam String surname,
                               @RequestParam int age,
                               @RequestParam String phone,
                               @RequestParam String email) {
        return clientService.updateClient(new Client(id, name, surname, age, phone, email));
    }

    @RequestMapping(method = RequestMethod.DELETE)
    public Client deleteClient(@RequestParam Long id) {
        return clientService.deleteClient(id);
    }
}

package com.kurilko.controllers;

import com.kurilko.domain.Product;
import com.kurilko.services.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import java.math.BigDecimal;

@Controller
@RequestMapping(value = "/mvc-products")
public class ProductController {
    @Autowired
    private ProductService productService;

    @RequestMapping(method = RequestMethod.GET)
    public String showProducts(ModelMap modelMap) {
        modelMap.put("message", productService.getProducts(0,50));
        return "products";
    }

    @RequestMapping(method = RequestMethod.POST)
    public Product addProduct(@RequestParam String name,
                            @RequestParam String price) {
        return productService.createProduct(new Product(name, new BigDecimal(price)));
    }

    @RequestMapping(method = RequestMethod.PUT)
    public Product modifyProduct(@RequestParam Long id,
                               @RequestParam String name,
                               @RequestParam String price) {
        return productService.updateProduct(new Product(id, name, new BigDecimal(price)));
    }

    @RequestMapping(method = RequestMethod.DELETE)
    public Product deleteProduct(@RequestParam Long id) {
        return productService.deleteProduct(id);
    }
}

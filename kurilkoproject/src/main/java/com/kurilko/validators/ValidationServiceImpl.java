package com.kurilko.validators;

import com.kurilko.dao.ClientDao;
import com.kurilko.domain.Client;
import com.kurilko.exceptions.BusinessException;

import java.util.List;

public class ValidationServiceImpl implements ValidationService {
    private ClientDao clientDao;

    private static final String regexpEmail = "^[A-Za-z0-9+_.-]+@(.+)$";

    public ValidationServiceImpl(ClientDao clientDao) {
        this.clientDao = clientDao;
    }

    @Override
    public void validateAge(int age) throws BusinessException {
        if (age < 0 || age > 200) {
            throw new BusinessException("Incorrect age!");
        }
    }

    @Override
    public void validatePhoneFormat(String phone) throws BusinessException {
        if (phone.length() != 10 && phone.length() != 12) {
            throw new BusinessException("Incorrect length of phone!");
        }
        if (!phone.matches("\\d+")) {
            throw new BusinessException("Incorrect format of phone! Please, input only digits and +!");
        }
    }

    @Override
    public void validateUniquenessOfPhone(String phone) throws BusinessException {
        List<Client> allClients = clientDao.getAllClients();
        if (allClients.stream().anyMatch(client -> client.getPhone().equals(phone))) {
            throw new BusinessException("A client with this phone number is already registered!");
        }
    }

    @Override
    public void validateEmail(String email) throws BusinessException {
        String testEmail = email.trim();
        if (!testEmail.matches(regexpEmail)) {
            throw new BusinessException("Wrong email format!");
        }
    }


}

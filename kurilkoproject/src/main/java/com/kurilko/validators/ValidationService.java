package com.kurilko.validators;

import com.kurilko.exceptions.BusinessException;

public interface ValidationService {
    void validateAge(int age) throws BusinessException;

    void validatePhoneFormat(String phone) throws BusinessException;

    void validateUniquenessOfPhone(String phone) throws BusinessException;

    void validateEmail(String email) throws BusinessException;
}

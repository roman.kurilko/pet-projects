package com.kurilko.validators;

import com.kurilko.dao.ClientDao;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class ValidatorsConfiguration {
    @Bean
    ValidationService validationService(ClientDao clientDao) {
        return new ValidationServiceImpl(clientDao);
    }
}

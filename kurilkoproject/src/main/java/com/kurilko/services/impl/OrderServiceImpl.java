package com.kurilko.services.impl;

import com.kurilko.dao.ClientDao;
import com.kurilko.dao.OrderDao;
import com.kurilko.dao.ProductDao;
import com.kurilko.domain.Client;
import com.kurilko.domain.Order;
import com.kurilko.domain.Product;
import com.kurilko.services.OrderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;

import java.text.ParseException;
import java.util.Collections;
import java.util.Date;
import java.util.List;

public class OrderServiceImpl implements OrderService {
    @Autowired
    @Qualifier(value = "orderEMDao")
    private OrderDao orderDao;

    @Autowired
    @Qualifier(value = "clientEMDao")
    private ClientDao clientDao;

    @Autowired
    private ProductDao productDao;

    public OrderServiceImpl(OrderDao orderDao) {
        this.orderDao = orderDao;
    }

    @Override
    public Order createOrder(Order order) {
        Order newOrder = orderDao.saveOrder(order);
        if (newOrder.getId() != 0) {
            System.out.println("Order " + newOrder.getId() + " saved!");
        } else {
            System.out.println("Error while creating new Order!");
        }
        return newOrder;
    }

    @Override
    public Order createOrder(Long client, String products, String date) {
        Client newClient = clientDao.getClient(client);
        List<Product> newProducts = productDao.getProducts(Conversion.stringArrayToLong(products.split(",")));
        Date newDate = null;
        try {
            newDate = Order.getDateFormatMDY().parse(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        Order newOrder = orderDao.saveOrder(new Order(newClient, newProducts, newDate));
        if (newOrder.getId() != 0) {
            System.out.println("Order " + newOrder.getId() + " saved!");
        } else {
            System.out.println("Error while creating new Order!");
        }
        return newOrder;
    }

    @Override
    public Order readOrder(Long id) {
        Order order = orderDao.getOrder(id);
        if (order.getId() != 0) {
            System.out.println("There is an order: " + order.toString() + " !");
        } else {
            System.out.println("Error while reading an order!");
        }
        return order;
    }

    @Override
    public List<Order> getOrders(Integer from, Integer to) {
        List<Order> orders = orderDao.getAllOrders();
        if (!orders.equals(Collections.emptyList())) {
            System.out.println("There is " + orders.size() + " orders!");
        } else {
            System.out.println("Error while reading list of orders!");
        }
        return orders;
    }

    @Override
    public List<Order> getOrdersById(Long clientId) {
        List<Order> orders = orderDao.getOrders(clientId);
        if (!orders.equals(Collections.emptyList())) {
            System.out.println("There is " + orders.size() + " orders!");
        } else {
            System.out.println("Error while reading list of orders!");
        }
        return orders;
    }

    @Override
    public Order updateOrder(Order order) {
        Order newOrder = orderDao.updateOrder(order);
        if (newOrder.getId() != 0) {
            System.out.println("Successfully updated: " + order.toString() + " !");
        } else {
            System.out.println("Error while updating an order!");
        }
        return newOrder;
    }

    @Override
    public Order updateOrder(Long id, Long client, String products, String date) {
        Client newClient = clientDao.getClient(client);
        List<Product> newProducts = productDao.getProducts(Conversion.stringArrayToLong(products.split(",")));
        Date newDate = null;
        try {
            newDate = Order.getDateFormatMDY().parse(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        Order newOrder = orderDao.updateOrder(new Order(id, newClient, newProducts, newDate));
        if (newOrder.getId() != 0) {
            System.out.println("Order " + newOrder.getId() + " saved!");
        } else {
            System.out.println("Error while creating new Order!");
        }
        return newOrder;
    }

    @Override
    public Order deleteOrder(Long id) {
        Order deletedOrder = orderDao.deleteOrder(id);
        if (deletedOrder.getId() != 0) {
            System.out.println("Successfully deleted: " + deletedOrder.toString() + " !");
        } else {
            System.out.println("Error while deleting an order!");
        }
        return deletedOrder;
    }
}

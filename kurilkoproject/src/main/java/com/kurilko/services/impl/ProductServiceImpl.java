package com.kurilko.services.impl;

import com.kurilko.dao.ProductDao;
import com.kurilko.domain.Product;
import com.kurilko.services.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;

import java.util.Collections;
import java.util.List;

public class ProductServiceImpl implements ProductService {
    @Autowired
    @Qualifier(value = "productEMDao")
    private ProductDao productDao;

    public ProductServiceImpl(ProductDao productDao) {
        this.productDao = productDao;
    }

    @Override
    public Product createProduct(Product product) {
        Product newProduct = productDao.saveProduct(product);
        if (newProduct.getId() != 0) {
            System.out.println("Product " + newProduct.getId() + " saved!");
        } else {
            System.out.println("Error while creating new Product!");
        }
        return newProduct;
    }

    @Override
    public Product readProduct(Long id) {
        Product product = productDao.getProduct(id);
        if (product.getId() != 0) {
            System.out.println("There is a product: " + product.toString() + " !");
        } else {
            System.out.println("Error while reading a product!");
        }
        return product;
    }

    @Override
    public List<Product> getProducts(Integer from, Integer to) {
        List<Product> products = productDao.getAllProducts();
        if (!products.equals(Collections.emptyList())) {
            System.out.println("There is " + products.size() + " products!");
        } else {
            System.out.println("Error while reading list of products!");
        }
        return products;
    }

    @Override
    public List<Product> getProducts(List<Long> ids) {
        List<Product> products = productDao.getProducts(ids);
        if (!products.equals(Collections.emptyList())) {
            System.out.println("There is " + products.size() + " products!");
        } else {
            System.out.println("Error while reading list of products!");
        }
        return products;
    }

    @Override
    public Product updateProduct(Product product) {
        Product newProduct = productDao.updateProduct(product);
        if (newProduct.getId() != 0) {
            System.out.println("Successfully updated: " + product.toString() + " !");
        } else {
            System.out.println("Error while updating a product!");
        }
        return newProduct;
    }

    @Override
    public Product deleteProduct(Long id) {
        Product deletedProduct = productDao.deleteProduct(id);
        if (deletedProduct.getId() != 0) {
            System.out.println("Successfully deleted: " + deletedProduct.toString() + " !");
        } else {
            System.out.println("Error while deleting a product!");
        }
        return deletedProduct;
    }
}

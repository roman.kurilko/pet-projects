package com.kurilko.services.impl;

import com.kurilko.dao.ClientDao;
import com.kurilko.domain.Client;
import com.kurilko.exceptions.BusinessException;
import com.kurilko.services.ClientService;
import com.kurilko.validators.ValidationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;

import java.util.Collections;
import java.util.List;

public class ClientServiceImpl implements ClientService {

    @Autowired
    @Qualifier(value = "clientEMDao")
    private ClientDao clientDao;
    @Autowired
    private ValidationService validationService;
    private static final String regexpPhone = "[\\-\\+\\(\\) ]";

    public ClientServiceImpl(ClientDao clientDao, ValidationService validationService) {
        this.clientDao = clientDao;
        this.validationService = validationService;
    }

    @Override
    public Client createClient(Client client) {
        try {
            client.setPhone(normalizePhoneNumber(client.getPhone()));
            validationService.validatePhoneFormat(client.getPhone());
            validationService.validateUniquenessOfPhone(client.getPhone());
        } catch (BusinessException e) {
            e.printStackTrace();
            System.out.println("Client isn't saved!\r\n");
            return new Client();
        }
        Client newClient = clientDao.saveClient(client);
        if (newClient == null) {
            System.out.println("Client doesn't saved! Wrong data!");
            return new Client();
        }
        System.out.println("Client " + newClient.getId() + " saved!");
        return newClient;
    }

    @Override
    public Client readClient(Long id) {
        Client client = clientDao.getClient(id);
        if (client == null) {
            System.out.println("There is no such client!");
            return new Client();
        }
        System.out.println("There is a client: " + client.toString() + " !");
        return client;
    }

    @Override
    public List<Client> getAllClients() {
        List<Client> listOfClients = clientDao.getAllClients();
        if (listOfClients.isEmpty()) {
            System.out.println("There are no clients!");
            return Collections.emptyList();
        }
        return listOfClients;
    }

    @Override
    public Client updateClient(Client client) {
        try {
            client.setPhone(normalizePhoneNumber(client.getPhone()));
            validationService.validatePhoneFormat(client.getPhone());
            validationService.validateUniquenessOfPhone(client.getPhone());
            validationService.validateEmail(client.getEmail());
            client.setEmail(client.getEmail());
        } catch (BusinessException e) {
            e.printStackTrace();
            System.out.println("Client isn't saved!\r\n");
            return new Client();
        }
        Client newClient = clientDao.updateClient(client);
        if (newClient == null) {
            System.out.println("Client doesn't modified! Wrong data!");
            return new Client();
        }
        return newClient;
    }

    @Override
    public Client deleteClient(Long id) {
        Client deletedClient = clientDao.deleteClient(id);
        if (deletedClient == null) {
            System.out.println("Client doesn't removed! Wrong data!");
            return new Client();
        }
        return deletedClient;
    }

    public static String normalizePhoneNumber(String phone) {
        phone = phone.trim().replaceAll(regexpPhone, "");
        return phone;
    }
}

package com.kurilko.services.impl;

import java.util.ArrayList;
import java.util.List;

public class Conversion {
    public static List<Long> stringArrayToLong(String[] ids) {
        List<Long> result = new ArrayList();
        for (int i = 0; i < ids.length; i++) {
            try {
                result.add(Long.parseLong(ids[i]));
            } catch (NumberFormatException e) {
                e.printStackTrace();
            }
        }
        return result;
    }
}

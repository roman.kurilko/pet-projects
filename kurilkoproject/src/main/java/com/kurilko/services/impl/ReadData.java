package com.kurilko.services.impl;

import com.kurilko.exceptions.BusinessException;
import com.kurilko.validators.ValidationService;

import java.io.BufferedReader;
import java.io.IOException;
import java.math.BigDecimal;

public class ReadData {
    public static BigDecimal readBigDecimal(BufferedReader br) {
        try {
            return new BigDecimal(br.readLine());
        } catch (IOException | NumberFormatException e) {
            System.out.println("Input a number please!");
            return readBigDecimal(br);
        }
    }

    public static int readInteger(BufferedReader br) {
        try {
            return Integer.parseInt(br.readLine());
        } catch (IOException | NumberFormatException e) {
            System.out.println("Input a number please!");
            return readInteger(br);
        }
    }

    public static Long readLong(BufferedReader br) {
        try {
            return Long.parseLong(br.readLine());
        } catch (IOException | NumberFormatException e) {
            System.out.println("Input a number please!");
            return readLong(br);
        }
    }

    public static String readString(BufferedReader br) {
        try {
            return br.readLine();
        } catch (IOException e) {
            e.printStackTrace();
            System.out.println("Input a string!");
            return readString(br);
        }
    }

    public static int readAge(BufferedReader br, ValidationService validationService) {
        try {
            int age = Integer.parseInt(br.readLine());
            validationService.validateAge(age);
            return age;
        } catch (IOException | NumberFormatException | BusinessException e) {
            System.out.println("Input a number please!");
            return readAge(br, validationService);
        }
    }
}

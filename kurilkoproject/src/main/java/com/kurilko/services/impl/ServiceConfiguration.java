package com.kurilko.services.impl;

import com.kurilko.dao.ClientDao;
import com.kurilko.dao.OrderDao;
import com.kurilko.dao.ProductDao;
import com.kurilko.services.ClientService;
import com.kurilko.services.OrderService;
import com.kurilko.services.ProductService;
import com.kurilko.validators.ValidationService;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class ServiceConfiguration {
    @Bean
    ClientService clientService(ClientDao clientDao, ValidationService validationService) {
        return new ClientServiceImpl(clientDao, validationService);
    }

    @Bean
    OrderService orderService(OrderDao orderDao) {
        return new OrderServiceImpl(orderDao);
    }

    @Bean
    ProductService productService(ProductDao productDao) {
        return new ProductServiceImpl(productDao);
    }
}

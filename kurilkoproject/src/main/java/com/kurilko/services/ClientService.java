package com.kurilko.services;

import com.kurilko.domain.Client;

import java.util.List;

public interface ClientService {
    /**
     * Method creates new client
     *
     * @param client is necessary client to create
     * @return created client to show somewhere
     */
    Client createClient(Client client);

    /**
     * Method allows us to get client
     *
     * @param id allows us to get client by id
     * @return instance of client that we wanted
     */
    Client readClient(Long id);

    /**
     * Method gets alli clients to show
     *
     * @return list of Client
     */
    List<Client> getAllClients();

    /**
     * Method updates existing client
     *
     * @param client is that changes, that we want to update
     * @return updated client to show
     */
    Client updateClient(Client client);

    /**
     * Method deletes client
     *
     * @param id allows us to delete a client
     * @return instance of client that we deleted to show
     */
    Client deleteClient(Long id);
}

package com.kurilko.services;

import com.kurilko.domain.Order;

import java.util.List;

public interface OrderService {
    /**
     * Method creates new order
     *
     * @param order is necessary order to create
     * @return created order to show somewhere
     */
    Order createOrder(Order order);

    /**
     * Method creates new order
     *
     * @return created order to show somewhere
     */
    Order createOrder(Long client, String products, String date);

    /**
     * Method allows us to get order
     *
     * @param id allows us to get order by id
     * @return instance of order that we wanted
     */
    Order readOrder(Long id);

    /**
     * Method allows us to get orders from some position to some position
     *
     * @param from is the start position of the list
     * @param to   is the end position of the list
     * @return list of Orders
     */
    List<Order> getOrders(Integer from, Integer to);

    /**
     * Method allows us to get orders by ID of client
     *
     * @param clientId ID of client
     * @return list of Orders
     */
    List<Order> getOrdersById(Long clientId);

    /**
     * Method updates existing order
     *
     * @param order is that changes, that we want to update
     * @return updated order to show
     */
    Order updateOrder(Order order);

    /**
     * Method updates existing order
     *
     * @return updated order to show
     */
    Order updateOrder(Long id, Long client, String products, String date);
    /**
     * Method deletes order
     *
     * @param id allows us to delete a order
     * @return instance of order that we deleted to show
     */
    Order deleteOrder(Long id);
}

package com.kurilko.services;

import com.kurilko.domain.Product;

import java.util.List;

public interface ProductService {
    /**
     * Method creates new product
     *
     * @param product is necessary product to create
     * @return created product to show somewhere
     */
    Product createProduct(Product product);

    /**
     * Method allows us to get product
     *
     * @param id allows us to get product by id
     * @return instance of product that we wanted
     */
    Product readProduct(Long id);

    /**
     * Method allows us to get products from some position to some position
     *
     * @param from is the start position of the list
     * @param to   is the end position of the list
     * @return list of Products
     */
    List<Product> getProducts(Integer from, Integer to);

    /**
     * Method allows us to get products from Array of ids
     *
     * @param ids is a collection of ids
     * @return list of Products
     */
    List<Product> getProducts(List<Long> ids);

    /**
     * Method updates existing product
     *
     * @param product is that changes, that we want to update
     * @return updated product to show
     */
    Product updateProduct(Product product);

    /**
     * Method deletes product
     *
     * @param id allows us to delete a product
     * @return instance of product that we deleted to show
     */
    Product deleteProduct(Long id);
}
